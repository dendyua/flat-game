
#pragma once

#include <memory>
#include <string>

#include <Flat/Core/Ref.hpp>
#include <Flat/Utils/Stream.hpp>

#include "Model/Common.hpp"
#include "Model/Protocol.hpp"
#include "Model/WatcherType.hpp"

#include "Sender.hpp"




namespace Flat { namespace Game {




class HandlerPrivate;

namespace Model {
	class Info;
	class Watcher;
	class Snapshot;
	class Storage;
	class ModelPrivate;
}




class FLAT_GAME_EXPORT Handler
{
public:
	struct Callbacks
	{
		virtual void sync() = 0;

		virtual void advanced() = 0;

		virtual void peerCreated(Model::PeerId peerId) = 0;
		virtual void peerDestroyed(Model::PeerId peerId) = 0;
		virtual void peerAdvanced(Model::PeerId peerId) = 0;

		virtual void routerError() = 0;
		virtual void peerError(Model::PeerId peerId) = 0;
	};

	Handler(const Model::Info & info, Callbacks & callbacks, const Model::Storage & storage,
			const Model::Snapshot & snapshot) noexcept;
	~Handler();

	void start(Flat::Utils::Stream & input);

	template <typename W>
	std::unique_ptr<W> installWatcher() noexcept;

	template <typename P>
	std::unique_ptr<Sender<P>> createSender(Model::PortId::Index portIndex, SenderOutput & output) noexcept;

	const HandlerPrivate & d() const noexcept;
	HandlerPrivate & d() noexcept;

private:
	std::unique_ptr<Model::Watcher> _installWatcher(Model::WatcherType watcherType) noexcept;
	void _uninstallWatcher(Model::Watcher & watcher) noexcept;

private:
	Ref<HandlerPrivate> d_;
};




template <typename W>
inline std::unique_ptr<W> Handler::installWatcher() noexcept
{
	static_assert(std::is_base_of<Model::Watcher, W>::value, "Must be derived from Watcher");
	auto watcher = _installWatcher(Model::WatcherTraits<W>::type());
	return std::unique_ptr<W>(static_cast<W*>(watcher.release()));
}

template <typename Protocol>
inline std::unique_ptr<Sender<Protocol>> Handler::createSender(
		const Model::PortId::Index portIndex, SenderOutput & output) noexcept
{
	return std::unique_ptr<Sender<Protocol>>(new Sender<Protocol>(*d_, portIndex, output));
}

inline const HandlerPrivate & Handler::d() const noexcept
{ return *d_; }

inline HandlerPrivate & Handler::d() noexcept
{ return *d_; }




}}
