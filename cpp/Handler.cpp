
#include "Handler.hpp"
#include "Handler_p.hpp"

#include <Flat/Utils/BufferStream.hpp>

#include "Debug.hpp"

#include "Model/Watcher.hpp"
#include "Model/Info.hpp"
#include "Model/Model.hpp"
#include "Model/Model_p.hpp"
#include "Model/Watcher_p.hpp"
#include "Model/Error_p.hpp"
#include "Model/Info_p.hpp"




namespace Flat { namespace Game {




HandlerPrivate::HandlerPrivate(Handler & handler, const Model::Info & info,
		Handler::Callbacks & callbacks, const Model::Storage & storage,
		const Model::Snapshot & snapshot) noexcept :
	handler_(handler),
	info_(info),
	callbacks_(callbacks),
	storage_(storage),
	snapshot_(snapshot),
	inputCallbacks_(*this)
{
	model_ = info_.createModel(handler_);
}


HandlerPrivate::~HandlerPrivate()
{
	FLAT_ASSERT(senderCount_ == 0);

	FLAT_ASSERT(watchers_.empty());

	if (input_) {
		input_->setReaderCallbacks(nullptr);
	}

	if (model_->isConstructed()) {
		model_->d().resetInput();
		input_ = nullptr;
	}

	model_.reset();
}


void HandlerPrivate::installSender(const Model::PortId::Index portIndex,
		SenderBase & sender) noexcept
{
	PortInfo & portInfo = portInfos_[static_cast<size_t>(portIndex)];
	FLAT_ASSERT(portInfo.sender == nullptr);

	portInfo.sender = &sender;
	senderCount_++;
}


void HandlerPrivate::uninstallSender(const Model::PortId::Index portIndex,
		SenderBase & sender) noexcept
{
	PortInfo & portInfo = portInfos_[static_cast<size_t>(portIndex)];
	FLAT_ASSERT(portInfo.sender == &sender);

	portInfo.sender = nullptr;
	senderCount_--;
}


Model::ProtocolId HandlerPrivate::protocolIdForPort(
		const Model::PortId::Index portIndex) const noexcept
{
	return model_->d().protocolIdForPort(portIndex);
}


void HandlerPrivate::sendPacket(const Model::PortId::Index portIndex,
		const Model::PacketId packetId, const Model::_PacketBase & packet,
		SenderOutput & output)
{
	const Model::ProtocolId protocolId = model_->d().protocolIdForPort(portIndex);
	const Model::Info::Private::PacketInfo & packetInfo =
			info_.d().packetInfo(protocolId, packetId);

	packetBuffer_.clear();
	auto writer = packetBuffer_.lockForWrite();
	// TODO: Detect proper size for packet id integral.
	writer->write(packetId);
	const int packetStartPos = writer->size();
	packetInfo.writer(*writer, packet);
	const int packetSize = static_cast<int>(writer->size()) - packetStartPos;
	writer = {};

	FLAT_ASSERT(packetSize >= packetInfo.size.min && packetSize <= packetInfo.size.max)
			<< "Invalid size written:" << packetSize << "expected min:"
			<< packetInfo.size.min << "max:" << packetInfo.size.max;

	output.begin();
	packetBuffer_.visit([&output] (const uint8_t * data, int size) {
		output.write(data, size); return true;
	});
	output.end();
}


void HandlerPrivate::start(Flat::Utils::Stream & input)
{
	if (snapshot_.isNull()) {
		model_->d().construct();
	} else {
//		model_->constructFromSnapshot(snapshot);
	}

	model_->d().setInput(input);

	input_ = &input;
	input_->setReaderCallbacks(&inputCallbacks_);
}


void HandlerPrivate::portCreated(const Model::PortId::Index portIndex) noexcept
{
	if (static_cast<size_t>(portIndex) >= portInfos_.size()) {
		portInfos_.resize(static_cast<size_t>(portIndex) + 1);
	}

	PortInfo & portInfo = portInfos_[static_cast<size_t>(portIndex)];
	FLAT_ASSERT(portInfo.sender == nullptr);
}


void HandlerPrivate::portDestroyed(const Model::PortId::Index portIndex) noexcept
{
	PortInfo & portInfo = portInfos_[static_cast<size_t>(portIndex)];
	FLAT_ASSERT(portInfo.sender == nullptr);
}


inline std::unique_ptr<Model::Watcher> HandlerPrivate::installWatcher(
		const Model::WatcherType watcherType) noexcept
{
	std::unique_ptr<Model::Watcher> watcher = info_.createWatcher(watcherType);

	watcher->d_->setModelPrivate(&model_->d());

	watchers_.insert(watcher.get());

	model_->d().watcherAdded(*watcher);

	return watcher;
}


inline void HandlerPrivate::uninstallWatcher(Model::Watcher & watcher) noexcept
{
	const auto it = watchers_.find(&watcher);
	FLAT_ASSERT(it != watchers_.end());

	model_->d().watcherRemoved(watcher);

	watcher.d_->setModelPrivate(nullptr);

	watchers_.erase(it);
}


void HandlerPrivate::peerCreated(const Model::PeerId peerId) noexcept
{
	callbacks_.peerCreated(peerId);
}


void HandlerPrivate::peerDestroyed(Model::PeerId peerId) noexcept
{
}


void HandlerPrivate::_readInput() noexcept
{
	std::exception_ptr error;

	try {
		while (true) {
			model_->d().processPacket();
			callbacks_.sync();
		}
	} catch (const Utils::Stream::IncompleteError &) {
		// thrown from processPacket() if not enough data in the input stream
	} catch (const std::exception & e) {
		FLAT_WARNING << "Error processing input:" << e;
		error = std::current_exception();
	}

	if (error) {
		try {
			std::rethrow_exception(error);
		} catch (const Model::PeerError & e) {
			callbacks_.peerError(e.peerId);
		} catch (const Model::RouterError &) {
			callbacks_.routerError();
		} catch (const std::exception & e) {
			FLAT_FATAL << "Unexpected exception:" << e;
		}
	}
}




Handler::Handler(const Model::Info & info, Callbacks & callbacks,
		const Model::Storage & storage,
		const Model::Snapshot & snapshot) noexcept :
	d_(*new HandlerPrivate(*this, info, callbacks, storage, snapshot))
{
}


Handler::~Handler()
{
}


void Handler::start(Flat::Utils::Stream & input)
{
	d_->start(input);
}


std::unique_ptr<Model::Watcher> Handler::_installWatcher(
		const Model::WatcherType watcherType) noexcept
{
	return d_->installWatcher(watcherType);
}


void Handler::_uninstallWatcher(Model::Watcher & watcher) noexcept
{
	return d_->uninstallWatcher(watcher);
}




}}
