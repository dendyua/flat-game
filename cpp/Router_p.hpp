
#pragma once

#include <vector>
#include <map>

#include "Router.hpp"

#include <Flat/Utils/IdGenerator.hpp>
#include <Flat/Utils/BufferStream.hpp>




namespace Flat { namespace Game {




class RouterPrivate;




static_assert(static_cast<int>(Model::PeerId::Null) == Utils::IdGenerator::kInvalidId);




class RouterPrivate
{
public:
	RouterPrivate(int peerLimit, int delayLimit, Router & router) noexcept;
	~RouterPrivate();

	Utils::Stream & output() noexcept;

	int peerCount() const noexcept;
	int delay() const noexcept;
	int delayForPeer(Model::PeerId peerId) const noexcept;

	SessionId currentSessionId() const noexcept;

	Session createSession() noexcept;
	void destroySession(const Session & session) noexcept;
	bool validateSession(const Session & session, bool advancing) const noexcept;
	void advanceSession(const Session & session) noexcept;
	void sendPeerPacket(const Session & session, const Model::PortId::Index portIndex,
			const uint8_t * data, int size) noexcept;
	void advancePeer(const Session & session) noexcept;

private:
	struct State
	{
		int peerCount;
	};

	struct SessionInfo
	{
		bool isDestroyed = false;
		Session session;
		std::uint32_t advanceFrameCounter = 0;
		std::uint32_t destroyFrame;

		std::uint32_t frame() const noexcept
		{ return static_cast<std::uint32_t>(session.sessionId) + advanceFrameCounter; }
	};

	struct PeerInfo
	{
		bool isNull() const noexcept
		{ return sessionInfo == nullptr; }

		SessionInfo * sessionInfo = nullptr;

		int delay = 0;
	};

private:
	SessionInfo * _sessionInfoForSession(const Session & session) const noexcept;

	PeerInfo & _peerInfo(Model::PeerId peerId) noexcept;
	const PeerInfo & _peerInfo(Model::PeerId peerId) const noexcept;

	void _resizePeersTo(Model::PeerId peerId) noexcept;

	void _advance() noexcept;
	void _advancePeer(PeerInfo & peerInfo) noexcept;

	template <typename T>
	Utils::Stream::Writer _writePacket(const T & packet);

private:
	const int delayLimit_;
	Router & router_;

	Utils::BufferStream output_;

	Utils::IdGenerator peerIdGenerator_;
	std::vector<PeerInfo> peerInfoForId_;

	std::vector<State> stateHistory_;

	std::uint64_t currentSessionFrame_ = 0;

	std::map<SessionId, std::unique_ptr<SessionInfo>> sessionInfoForId_;
};




inline Utils::Stream & RouterPrivate::output() noexcept
{ return output_; }

inline int RouterPrivate::peerCount() const noexcept
{ return peerIdGenerator_.count(); }

inline SessionId RouterPrivate::currentSessionId() const noexcept
{ return static_cast<SessionId>(currentSessionFrame_); }

inline RouterPrivate::PeerInfo & RouterPrivate::_peerInfo(const Model::PeerId peerId) noexcept
{ return peerInfoForId_[static_cast<size_t>(peerId)]; }

inline const RouterPrivate::PeerInfo & RouterPrivate::_peerInfo(
		const Model::PeerId peerId) const noexcept
{ return peerInfoForId_.at(static_cast<size_t>(peerId)); }




}}
