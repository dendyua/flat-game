
#pragma once

#include <Flat/Core/Ref.hpp>
#include <Flat/Utils/Stream.hpp>




namespace Flat { namespace Game { namespace Model {




class CorrectionPrivate;




class FLAT_GAME_EXPORT Correction
{
public:
	typedef uint32_t Result;

	Correction();
	~Correction();

	Flat::Utils::Stream::Writer stream();

	template <typename T>
	Correction & operator<<(const T & value);

	Result finish();

private:
	Ref<CorrectionPrivate> d_;
};




template <typename T>
inline Correction & Correction::operator<<(const T & value)
{ stream() << value; return *this; }




}}}
