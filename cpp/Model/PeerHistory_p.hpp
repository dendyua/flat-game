
#pragma once

#include <vector>

#include "Debug.hpp"




namespace Flat { namespace Game { namespace Model {




template <typename T>
class PeerHistory
{
public:
	PeerHistory(int limit) noexcept;

	int limit() const noexcept;
	int count() const noexcept;

	void push(const T & value) noexcept;
	void pop() noexcept;

	const T & at(int index) const noexcept;
	T & operator[](int index) noexcept;

private:
	inline int _offsetForIndex(int index) const noexcept;

private:
	std::vector<T> values_;
	int first_ = 0;
	int count_ = 0;
};




template <typename T>
inline PeerHistory<T>::PeerHistory(const int limit) noexcept :
	values_(limit)
{}

template <typename T>
inline int PeerHistory<T>::limit() const noexcept
{ return static_cast<int>(values_.size()); }

template <typename T>
inline int PeerHistory<T>::count() const noexcept
{ return count_; }

template <typename T>
inline void PeerHistory<T>::push(const T & value) noexcept
{
	FLAT_ASSERT(count_ < limit());
	const int index = (first_ + count_ + 1) % limit();
	values_[index] = value;
	count_++;
}

template <typename T>
inline void PeerHistory<T>::pop() noexcept
{
	FLAT_ASSERT(count_ > 0);
	values_[first_] = T();
	first_ = (first_ + 1) % limit();
	count_--;
}

template <typename T>
const T & PeerHistory<T>::at(const int index) const noexcept
{ return values_.at(_offsetForIndex(index)); }

template <typename T>
inline T & PeerHistory<T>::operator[](const int index) noexcept
{ return values_[_offsetForIndex(index)]; }

template <typename T>
inline int PeerHistory<T>::_offsetForIndex(const int index) const noexcept
{
	FLAT_ASSERT(index < limit());
	return (first_ + count_ - index - 1 + limit()) % limit();
}




}}}
