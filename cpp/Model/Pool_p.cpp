
#include "Pool_p.hpp"

#include "PeerPool_p.hpp"




namespace Flat { namespace Game { namespace Model {




static const int kDefaultEnlargeCount = 16;




PoolBase::PoolBase(const PeerPool & peerPool, const int limit, const int reserve,
		const int delayLimit, const FreeCallback freeCallback) noexcept :
	peerPool_(peerPool),
	limit_(limit),
	reserve_(reserve),
	delayLimit_(delayLimit),
	freeCallback_(freeCallback)
{
	enlargeCount_ = kDefaultEnlargeCount;

	firstFree_ = Index::Null;
	firstTaken_ = Index::Null;
	firstZombie_ = Index::Null;
	lastZombie_ = Index::Null;

	takenCount_ = 0;
	zombieCount_ = 0;

	timestamp_ = 0;
}


inline int PoolBase::_freeCount() const noexcept
{
	FLAT_ASSERT(takenCount_ >= 0 && takenCount_ <= limit_);

	const int freeCount = limit_ - takenCount_;
	FLAT_ASSERT(freeCount >= 0 && freeCount <= limit_);

	return freeCount;
}


void PoolBase::_freeZombiesAtDelay(const int delay) noexcept
{
	for (PoolIteratorBase<IdMode::Zombie> it(*this); !it.atEnd();) {
		const Index index = it.next();
		IdInfo & id = _at(index);
		FLAT_ASSERT(id.mode == IdMode::Zombie);
		const int zombieDelay = static_cast<int>(timestamp_ - id.zombieTimestamp);
		FLAT_ASSERT(zombieDelay >= 0 && zombieDelay <= delay);
		if (zombieDelay < delay) break;

		firstZombie_ = id.next;
		zombieCount_--;

		id.next = firstFree_;
		firstFree_ = index;

		if (freeCallback_) {
			freeCallback_(index);
		}
	}
}


void PoolBase::peerRemoved(const PeerId peerId) noexcept
{
	const int peerDelay = peerPool_.delayForPeer(peerId);

	for (int delay = peerDelay - 1; delay >= 0; delay--) {
		if (peerPool_.peerCountAtDelay(delay) > 1) break;
		_freeZombiesAtDelay(delay);
	}
}


void PoolBase::advance() noexcept
{
	timestamp_++;
}


void PoolBase::advancePeer(const PeerId peerId) noexcept
{
	const int delay = peerPool_.delayForPeer(peerId);
	FLAT_ASSERT(delay > 0);

	if (peerPool_.peerCountAtDelay(delay) > 1) return;
	FLAT_ASSERT(delay == peerPool_.delay());

	_freeZombiesAtDelay(delay);
}


void PoolBase::_enlarge() noexcept
{
	const Index beginIndex = static_cast<Index>(ids_.size());
	ids_.resize(std::min(static_cast<int>(beginIndex) + enlargeCount(), _totalLimit()));

	for (Index index = beginIndex; index < static_cast<Index>(ids_.size());) {
		IdInfo & id = _at(index);
		index = static_cast<Index>(static_cast<int>(index) + 1);
		id.mode = IdMode::Free;
		id.next = index;
	}

	const Index endIndex = static_cast<Index>(ids_.size() - 1);
	_at(endIndex).next = firstFree_;

	firstFree_ = beginIndex;
}


PoolBase::Index PoolBase::take() noexcept
{
	FLAT_ASSERT(takenCount_ < limit_);

	if (firstFree_ == Index::Null) {
		_enlarge();
	}

	const Index index = firstFree_;
	IdInfo & id = _at(firstFree_);

	firstFree_ = id.next;

	if (firstTaken_ != Index::Null) {
		_at(firstTaken_).previous = index;
	}

	id.mode = IdMode::Taken;
	id.previous = Index::Null;
	id.next = firstTaken_;
	id.takenTimestamp = timestamp_;
	firstTaken_ = index;

	takenCount_++;

	return index;
}


void PoolBase::free(const Index index) noexcept
{
	IdInfo & id = _at(index);
	FLAT_ASSERT(id.mode == IdMode::Taken);

	// switch mode from Taken to Zombie
	FLAT_ASSERT(id.mode == IdMode::Taken);

	takenCount_--;

	if (peerPool_.count() == 0) {
		// free instantly
		id.mode = IdMode::Free;
		id.next = firstFree_;
		firstFree_ = index;
		if (freeCallback_) freeCallback_(index);
		return;
	}

	// mark as zombie
	id.mode = IdMode::Zombie;
	id.zombieTimestamp = timestamp_;
	id.next = firstZombie_;
	firstZombie_ = index;

	zombieCount_++;
}


int PoolBase::count() const noexcept
{
	return takenCount_;
}


int PoolBase::availableCount() const noexcept
{
	return limit_ - takenCount_;
}


int PoolBase::reserveCount() const noexcept
{
	const int overload = std::min(0, reserve_ - zombieCount_);
	return limit_ - takenCount_ + overload;
}


bool PoolBase::validate(const PeerId peerId, const Index index) const noexcept
{
	const int i = static_cast<int>(index);

	FLAT_ASSERT(i >= 0 && i < _totalLimit());

	if (i >= static_cast<int>(ids_.size())) return false;

	const IdInfo & id = _at(index);
	if (id.mode == IdMode::Free) return false;

	const int peerDelay = peerPool_.delayForPeer(peerId);

	const int takenDelay = static_cast<int>(timestamp_ - id.takenTimestamp);

	if (id.mode == IdMode::Taken) {
		return peerDelay <= takenDelay;
	}

	FLAT_ASSERT(id.mode == IdMode::Zombie);
	const int zombieDelay = static_cast<int>(timestamp_ - id.zombieTimestamp);

	return peerDelay <= takenDelay && peerDelay > zombieDelay;
}


bool PoolBase::isZombie(const Index index) const noexcept
{
	const IdInfo & id = _at(index);
	FLAT_ASSERT(id.mode == IdMode::Taken || id.mode == IdMode::Zombie);
	return id.mode == IdMode::Zombie;
}




#if 0
QDataStream & operator<<( QDataStream & ds, const IdGenerator & g )
{
	const IdGenerator::Id * const ids = g.ids_.constData();

	const int size = g.ids_.size();
	ds << (quint32)size;

	// save free ids order
	{
		ds << (quint32)g.firstFree_;
		for ( int i = g.firstFree_; i != 0; i = ids[ i ].next )
			ds << (quint32)ids[ i ].next;
	}

	// save taken ids order
	{
		ds << (quint32)g.firstTaken_;
		for ( int i = g.firstTaken_; i != 0; i = ids[ i ].next )
			ds << (quint32)ids[ i ].next;
	}

	// save zombie ids order
	{
		ds << (quint32)g.firstZombie_;
		for ( int i = g.firstZombie_; i != 0; i = ids[ i ].next )
			ds << (quint32)ids[ i ].next;
	}

	// save transmitters
	ds << (quint32)g.transmitterIds_.count();

	for ( QListIterator<int> transmittersIt( g.transmitterIds_ ); transmittersIt.hasNext(); )
	{
		const IdGenerator::Transmitter & transmitter = g.transmitters_.at( transmittersIt.next() );

		ds << (quint32)transmitter.transmitterId;

		ds << (quint32)transmitter.zombieLists.count();
		for ( QListIterator<IdGenerator::Transmitter::ZombieList> zombieListIt( transmitter.zombieLists ); zombieListIt.hasNext(); )
		{
			const IdGenerator::Transmitter::ZombieList & zombieList = zombieListIt.next();

			ds << (quint32)zombieList.count();
			for ( QListIterator<int> zombieIt( zombieList ); zombieIt.hasNext(); )
			{
				const int zombie = zombieIt.next();
				ds << (quint32)zombie;
			}
		}
	}

	return ds;
}


bool IdGenerator::_readDataStream( QDataStream & ds, IdGenerator & g )
{
	FLAT_ASSERT(g.limit_ != -1);
	FLAT_ASSERT(g.zombieLimit_ != -1);
	FLAT_ASSERT(g.peerLimit_ != -1);

	const int maxSize = 1 + g.limit_ + g.zombieLimit_;

	const int size = dsr.readUint32();
	if ( ds.status() != QDataStream::Ok )
		return true;

	if ( size <= 0 || size > maxSize )
		return false;

	std::vector<IdGenerator::Id> localIds;
	localIds.reserve( std::min( size + g.enlargeCount_, maxSize ) );
	localIds.resize( size );
	IdGenerator::Id * const ids = localIds.data();

	// clear
	{
		for ( int id = 1; id < size; ++id )
		{
			ids[ id ].zombieCount = -1;
			ids[ id ].takeIteration = 0;
		}
	}

	// read free ids
	const int firstFree = dsr.readUint32();
	if ( ds.status() != QDataStream::Ok )
		return true;

	int freeCount = 0;

	{
		int previous = 0;
		for ( int id = firstFree; id != 0; )
		{
			// take next free id
			const int next = dsr.readUint32();
			if ( ds.status() != QDataStream::Ok )
				return true;

			// check for out of bounds
			if ( id < 0 || id >= size )
				return false;

			// check for duplicated free id
			if ( ids[ id ].zombieCount != -1 )
				return false;

			ids[ id ].isFree = true;
			ids[ id ].zombieCount = 0;
			ids[ id ].next = next;
			ids[ id ].previous = previous;

			previous = id;
			id = next;

			freeCount++;
		}
	}

	// read taken ids
	const int firstTaken = dsr.readUint32();
	if ( ds.status() != QDataStream::Ok )
		return true;

	int takenCount = 0;

	{
		int previous = 0;
		for ( int id = firstTaken; id != 0; )
		{
			// take next taken id
			const int next = dsr.readUint32();
			if ( ds.status() != QDataStream::Ok )
				return true;

			// check for out of bounds
			if ( id < 0 || id >= size )
				return false;

			// check for duplicated free id
			if ( ids[ id ].zombieCount != -1 )
				return false;

			ids[ id ].isFree = false;
			ids[ id ].zombieCount = 0;
			ids[ id ].next = next;
			ids[ id ].previous = previous;

			previous = id;
			id = next;

			takenCount++;
		}
	}

	// read zombie ids
	const int firstZombie = dsr.readUint32();
	if ( ds.status() != QDataStream::Ok )
		return true;

	int zombieCount = 0;

	{
		int previous = 0;
		for ( int id = firstZombie; id != 0; )
		{
			// take next taken id
			const int next = dsr.readUint32();
			if ( ds.status() != QDataStream::Ok )
				return true;

			// check for out of bounds
			if ( id < 0 || id >= size )
				return false;

			// check for duplicated free id
			if ( ids[ id ].zombieCount != -1 )
				return false;

			ids[ id ].isFree = true;
			ids[ id ].zombieCount = -2;
			ids[ id ].next = next;
			ids[ id ].previous = previous;

			previous = id;
			id = next;

			zombieCount++;
		}
	}

	if ( 1 + freeCount + takenCount + zombieCount != localIds.size() )
		return false;

	// read transmitters
	std::vector<IdGenerator::Transmitter> transmitters;
	std::vector<int> transmitterIds;

	int transmittersZombieCount = 0;

	{
		const int transmittersCount = dsr.readUint32();
		if ( ds.status() != QDataStream::Ok )
			return true;

		if ( transmittersCount < 0 || transmittersCount > g.transmittersLimit() )
			return false;

		for ( int id = 0; id < transmittersCount; ++id )
		{
			const int transmitterId = dsr.readUint32();
			if ( ds.status() != QDataStream::Ok )
				return true;

			if ( transmitterId <= 0 || transmitterId > g.transmittersLimit() )
				return false;

			if ( transmitterId >= transmitters.size() )
			{
				if ( transmitterId >= transmitters.capacity() )
					transmitters.reserve( transmitterId + 1 + 16 );
				transmitters.resize( transmitterId + 1 );
			}

			IdGenerator::Transmitter & transmitter = transmitters[ transmitterId ];

			// check for duplicated transmitter id
			if ( !transmitter.isNull() )
				return false;

			transmitterIds << transmitterId;

			transmitter.transmitterId = transmitterId;

			const int zombieListsCount = dsr.readUint32();
			if ( ds.status() != QDataStream::Ok )
				return true;

			if ( zombieListsCount < 0 )
				return false;

			for ( int zl = 0; zl < zombieListsCount; ++zl )
			{
				IdGenerator::Transmitter::ZombieList zombieList;

				const int zombieCount = dsr.readUint32();
				if ( ds.status() != QDataStream::Ok )
					return true;

				if ( zombieCount < 0 || zombieCount >= size )
					return false;

				for ( int z = 0; z < zombieCount; ++z )
				{
					const int id = dsr.readUint32();
					if ( ds.status() != QDataStream::Ok )
						return true;

					if ( id <= 0 || id >= size )
						return false;

					IdGenerator::Id & zombieId = ids[ id ];

					if ( zombieId.zombieCount == 0 )
					{
						// this one was already marked as free or taken
						return false;
					}

					FLAT_ASSERT(zombieId.zombieCount == -2 || zombieId.zombieCount > 0);

					if ( zombieId.zombieCount == -2 )
					{
						zombieId.zombieCount = 1;
						transmittersZombieCount++;
					}
					else
					{
						zombieId.zombieCount++;
					}

					zombieId.isFree = true;
					zombieList << id;
				}

				transmitter.zombieLists << zombieList;
			}
		}
	}

	if ( transmittersZombieCount != zombieCount )
		return false;

	g.ids_ = localIds;

	g.firstFree_ = firstFree;
	g.firstTaken_ = firstTaken;
	g.firstZombie_ = firstZombie;

	g.freeCount_ = freeCount;
	g.zombieCount_ = zombieCount;

	g.transmitterIds_ = transmitterIds;
	g.transmitters_ = transmitters;

	return true;
}


QDataStream & operator>>( QDataStream & ds, IdGenerator & g )
{
	if ( ds.status() != QDataStream::Ok )
		return ds;

	if ( !IdGenerator::_readDataStream( ds, g ) )
		ds.setStatus( QDataStream::ReadCorruptData );

	return ds;
}
#endif




}}}
