
#pragma once

#include "Info.hpp"




namespace Flat { namespace Game {
	class Handler;
}}




namespace Flat { namespace Game { namespace Model {




class ContextPrivate : public Context
{
public:
	ContextPrivate(const Info & info, Handler & handler) noexcept :
		info(info),
		handler(handler)
	{}

	const Info & info;
	Handler & handler;
};




}}}
