
#include "PeerPool_p.hpp"

#include "Debug.hpp"
#include "Info.hpp"




namespace Flat { namespace Game { namespace Model {




constexpr int kMaxPeer = 64;




inline int PeerPool::_limit() const noexcept
{ return info_.peerLimit(); }




PeerPool::Iterator::Iterator(const PeerPool & pool) noexcept :
	pool_(pool),
	next_(pool.first_)
{
}


bool PeerPool::Iterator::atEnd() const noexcept
{
	return next_ == PeerId::Null;
}


PeerId PeerPool::Iterator::next()
{
	FLAT_ASSERT(!atEnd());
	const PeerId value = next_;
	next_ = pool_.peerInfos_.at(static_cast<int>(value)).next;
	return value;
}




PeerPool::PeerPool(const Info & info) noexcept :
	info_(info),
	states_(info.delayLimit())
{
}


void PeerPool::create(const PeerId id) noexcept
{
	const int index = static_cast<int>(id);
	FLAT_ASSERT(index >= 0 && index < _limit());

	_enlarge(index);

	PeerInfo & peerInfo = peerInfos_[index];
	FLAT_ASSERT(peerInfo.isNull);

	peerInfo.previous = PeerId::Null;
	peerInfo.next = first_;
	peerInfo.isNull = false;
	first_ = id;

	count_++;
}


void PeerPool::destroy(const PeerId id) noexcept
{
	const int index = static_cast<int>(id);
	FLAT_ASSERT(index >= 0 && index < _limit() && index < static_cast<int>(peerInfos_.size()));
	PeerInfo & peerInfo = peerInfos_[index];
	FLAT_ASSERT(!peerInfo.isNull);

	if (peerInfo.previous != PeerId::Null) {
		peerInfos_[static_cast<int>(peerInfo.previous)].next = peerInfo.next;
	}

	if (peerInfo.next != PeerId::Null) {
		peerInfos_[static_cast<int>(peerInfo.next)].previous = peerInfo.previous;
	}

	peerInfo = PeerInfo();
	peerInfo.previous = PeerId::Null;
	peerInfo.next = firstNull_;
	firstNull_ = id;

	count_--;
}


bool PeerPool::isValid(const PeerId id) const noexcept
{
	const int index = static_cast<int>(id);
	if (index < 0 || index >= static_cast<int>(peerInfos_.size())) return false;
	const PeerInfo & peerInfo = peerInfos_.at(index);
	return !peerInfo.isNull;
}


void PeerPool::advance() noexcept
{
	for (Iterator it(*this); !it.atEnd();) {
		PeerInfo & peerInfo = _infoForPeer(it.next());
		FLAT_ASSERT(peerInfo.delay < info_.delayLimit());
		peerInfo.delay++;
	}

	State state;
	state.peerCount = count_;
	states_.push(state);
}


void PeerPool::advancePeer(const PeerId id) noexcept
{
	PeerInfo & peerInfo = _infoForPeer(id);
	FLAT_ASSERT(!peerInfo.isNull);
	FLAT_ASSERT(peerInfo.delay > 0);

	State & state = states_[peerInfo.delay];
	FLAT_ASSERT(state.peerCount > 0);
	if (state.peerCount == 1) {
		FLAT_ASSERT(peerInfo.delay == states_.count() - 1);
		states_.pop();
	} else {
		FLAT_ASSERT(peerInfo.delay < states_.count() - 1);
		state.peerCount--;
	}

	peerInfo.delay--;
}


void PeerPool::_enlarge(const int maxIndex) noexcept
{
	if (maxIndex < static_cast<int>(peerInfos_.size())) return;

	const int beginIndex = static_cast<int>(peerInfos_.size());
	peerInfos_.resize(maxIndex + 1);

	for (int index = beginIndex; index < static_cast<int>(peerInfos_.size()); ++index) {
		PeerInfo & peerInfo = peerInfos_[index];
		FLAT_ASSERT(peerInfo.isNull);
		peerInfo.next = firstNull_;
		peerInfo.previous = PeerId::Null;
	}

	firstNull_ = static_cast<PeerId>(peerInfos_.size() - 1);
}




}}}
