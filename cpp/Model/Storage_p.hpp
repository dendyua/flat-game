
#pragma once

#if 0 // disabled until replacement added in Qt5
#include <QtCore/5.0.0/QtCore/private/qabstractfileengine_p.h>
#endif

#include <QString>
#include <QReadWriteLock>




namespace Flat { namespace Game { namespace Model {




extern const QString kFileSystemSchemePrefix;




class Storage;




class FileSystemThreadCache
{
public:
	bool isManagerDisabled = false;
};


extern FileSystemThreadCache * fileSystemThreadCache();




class FileSystemPrivate
{
public:
	FileSystemPrivate(FileSystem & fileSystem);
	~FileSystemPrivate();

	void setMounted(bool set);

private:
	FileSystem & fileSystem_;

	QString mountPoint_;

	bool isMounted_ = false;

	friend class FileSystem;
	friend class FileSystemManager;
};




#if 0
class FileSystemEngineHandler : public QAbstractFileEngineHandler
{
public:
	QAbstractFileEngine * create( const QString & fileName ) const;
};




class FileSystemEngine : public QAbstractFileEngine
{
public:
	FileSystemEngine( const QString & fileName, QAbstractFileEngine * engine );

	bool supportsExtension( Extension extension ) const;
	bool extension( Extension extension, const ExtensionOption * option, ExtensionReturn * output );

	Iterator * beginEntryList( QDir::Filters filters, const QStringList & filterNames );

	bool caseSensitive() const;
	FileFlags fileFlags( FileFlags type ) const;
	QString fileName( FileName file ) const;
	QDateTime fileTime( FileTime time ) const;
	bool isRelativePath() const;
	bool isSequential() const;

	bool open( QIODevice::OpenMode openMode );
	bool close();
	bool flush();

	bool copy( const QString & newName );

	QStringList entryList( QDir::Filters filters, const QStringList & filterNames ) const;

private:
	QString fileName_;
	QAbstractFileEngine * engine_;
};




class FileSystemManager
{
public:
	static FileSystemManager * sharedManager();

	FileSystemManager();
	~FileSystemManager();

	void mount( FileSystem * fileSystem );
	void unmount( FileSystem * fileSystem );

	// called from GameFileSystemEngineHandler
	QAbstractFileEngine * create( const QString & fileName ) const;

private:
	FileSystemEngineHandler * fileSystemEngineHandler_;

	mutable QReadWriteLock fileSystemsMutex_;
	int mountPointCounter_;
	QList<FileSystem*> fileSystems_;
};
#endif




}}}
