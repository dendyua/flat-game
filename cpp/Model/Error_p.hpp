
#pragma once

#include <stdexcept>

#include "Common.hpp"




namespace Flat { namespace Game { namespace Model {




class Error : public std::runtime_error
{
public:
	Error() noexcept : std::runtime_error(nullptr) {}
};




class RouterError : public Error
{
};




class PeerError : public Error
{
public:
	explicit PeerError(PeerId peerId) noexcept : peerId(peerId) {}
	const PeerId peerId;
};




}}}
