
#pragma once

#include <Flat/Utils/IdGenerator.hpp>
#include <Flat/Utils/Stream.hpp>

#include "Model.hpp"
#include "Snapshot.hpp"
#include "Snapshot_p.hpp"




namespace Flat { namespace Game {
	class HandlerPrivate;
}}




namespace Flat { namespace Game { namespace Model {




class Info;




#if 0
class Model::ProtocolHandler::Private
{
public:
	Private(_ProtocolToken protocolToken, Model::_ProtocolHandlerId protocolHandlerId) noexcept :
		protocolToken(protocolToken),
		protocolHandlerId(protocolHandlerId)
	{}

	const _ProtocolToken protocolToken;
	const Model::_ProtocolHandlerId protocolHandlerId;
	std::vector<Model::PacketHandler> packetHandlers;
};
#endif




class ModelPrivate
{
public:
	ModelPrivate(Model & model, const Info & info, Handler & handler) noexcept;
	~ModelPrivate();

	const Model & model() const noexcept;
	Model & model() noexcept;

	HandlerPrivate & handlerPrivate() noexcept;

	const Storage & storage() const noexcept;

	ProtocolId protocolIdForPort(PortId::Index portIndex) const noexcept;

	// construction
	bool isConstructed() const noexcept;
	void construct();
	void constructFromSnapshot(const Snapshot & snapshot);

	void setInput(Flat::Utils::Stream & input) noexcept;
	void resetInput() noexcept;
	void processPacket();

	void watcherAdded(Watcher & watcher) noexcept;
	void watcherRemoved(Watcher & watcher) noexcept;

	int availablePortCount() const noexcept;
	int reservePortCount() const noexcept;
	PortId allocatePort(ProtocolId protocolId, Model::_ProtocolHandlerId handlerId,
			int count) noexcept;
	void registerPortPacketHandler(PortId::Index portIndex, PacketId packetId,
			Model::PacketHandler packetHandler) noexcept;
	void destroyPort(PortId::Index index) noexcept;

	int availableIdCount() const noexcept;
	int reserveIdCount() const noexcept;
	Id takeId() noexcept;
	void freeId(Id::Index index) noexcept;

	Correction & correction() noexcept;

#if 0
	bool bindPort( int portId, int protocolId );
	bool bindIdGenerator( int generatorId );
	bool bindId( int generatorId, int id );
#endif

#if 0
	// packets
	void readPacket(PortId::Index portIndex, int packetId, QDataStream & data) noexcept;
#endif

	bool isAdvanced() const noexcept;
	void markAdvanced() noexcept;

	void advance();

	void peerCreated(PeerId peerId) noexcept;
	void peerDestroyed(PeerId peerId) noexcept;
	void peerAdvanced(PeerId peerId) noexcept;

	// helpers
	int delay() const noexcept;
	int delayForPeer(int peerId) const noexcept;

#if 0
	// snapshoting
	Snapshot makeSnapshot() const;

	static void initSnapshotDataStream(QDataStream & ds);
	static void writeSnapshot(QDataStream & ds, const Snapshot & s, const StreamPrivate * threadPrivate);
	static void readSnapshot(QDataStream & ds, Snapshot & s, const StreamPrivate * threadPrivate);
#endif

	void peerCreatedArrived(PeerId peerId);
	void peerDestroyedArrived(PeerId peerId);
	void peerPortPacketArrived(PeerId peerId, PortId::Index portIndex, int packetSize, Utils::Stream::ReaderIf & reader);
	void peerAdvancedArrived(PeerId peerId);

#if 0
private:
	static void _writeSnapshotDataHeader(QDataStream & ds, const Data & s,
			const HandlerManagerPrivate * handlerManagerPrivate);
	static void _writeSnapshotDataPendingPackets(QDataStream & ds, const Data & s,
			const HandlerManagerPrivate * handlerManagerPrivate);

	static bool _readSnapshotDataHeaderInternal(QDataStream & ds, Data & s,
			const StreamPrivate * threadPrivate);
	static bool _readSnapshotDataPendingPacketsInternal(QDataStream & ds, Data & s,
			const StreamPrivate * threadPrivate);
	static bool _readSnapshotInternal(QDataStream & ds, Snapshot & s,
			const StreamPrivate * threadPrivate);
#endif

private:
	class PortInfo
	{
	public:
		bool isNull() const noexcept
		{ return portIndex == PortId::Null; }

		PortId::Index portIndex = PortId::Null;

		Model::_ProtocolHandlerId protocolHandlerId = nullptr;
		std::vector<Model::PacketHandler> packetHandlers;
	};

#if 0
	class ProtocolInfo
	{
	public:
		bool isNull() const
		{ return protocolId == 0; }

		int protocolId = 0;

		// protocol
		ProtocolRef * protocolRef = nullptr;
		ProtocolDestroyFunction destroy;

		// handler
		ProtocolHandlerRef * protocolHandlerRef;
		ProtocolHandlePacketFunction handlePacket;
	};
#endif

private:
	void _freePort(PortPool::Index index) noexcept;

	void _resizePortsTo(PortId::Index index) noexcept;
	PortInfo & _portInfo(PortId::Index index) noexcept;

	void _processPacket(Flat::Utils::Stream::ReaderIf & reader);

private:
	// runtime
	Model & model_;
	const Info & info_;
	Handler & handler_;

	bool isConstructed_ = false;

	Flat::Utils::Stream * input_ = nullptr;

#if 0
	// protocols
	std::vector<ProtocolInfo> protocolInfos_;
#endif

	// snapshot helpers
	bool isConstructingFromSnapshot_;
	Utils::IdGenerator portValidator_;
	Utils::IdGenerator idGeneratorValidator_;
	std::vector<Utils::IdGenerator> idValidatorForGeneratorId_;

	// snapshot
	Data data_;

	// protocol
	std::vector<PortInfo> portInfos_;

	// correction
	Correction correction_;

	// runtime helpers
	bool isAdvanced_;
	bool isPeerCorrupted_;
};




inline const Model & ModelPrivate::model() const noexcept
{ return model_; }

inline Model & ModelPrivate::model() noexcept
{ return model_; }

inline ProtocolId ModelPrivate::protocolIdForPort(const PortId::Index portIndex) const noexcept
{ return data_.portInfoAt(portIndex).protocolId; }

inline bool ModelPrivate::isConstructed() const noexcept
{ return isConstructed_; }

inline bool ModelPrivate::isAdvanced() const noexcept
{ return isAdvanced_; }

inline void ModelPrivate::markAdvanced() noexcept
{ isAdvanced_ = true; }

inline Correction & ModelPrivate::correction() noexcept
{ return correction_; }

inline ModelPrivate::PortInfo & ModelPrivate::_portInfo(const PortId::Index index) noexcept
{ return portInfos_[static_cast<int>(index)]; }



}}}
