
#pragma once

#include <any>

#include "Data_p.hpp"




namespace Flat { namespace Game { namespace Model {




class SnapshotPrivate
{
public:
	SnapshotPrivate(const Data & data, const std::any & snapshot) :
		data(data),
		snapshot(snapshot)
	{}

	Data data;
	std::any snapshot;
};




}}}
