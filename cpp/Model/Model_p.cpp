
#include "Model_p.hpp"

#include "Info.hpp"
#include "Info_p.hpp"
#include "Error_p.hpp"
#include "RouterProtocol_p.hpp"

#include "../Handler.hpp"
#include "../Handler_p.hpp"




namespace Flat { namespace Game { namespace Model {




constexpr int kVectorExtent = 16;

template <typename Index, typename Vector>
static void reserveVectorIndex(Vector & v, Index index)
{
	if (static_cast<int>(index) < static_cast<int>(v.size())) return;

	if (static_cast<int>(index) >= static_cast<int>(v.capacity())) {
		v.reserve(static_cast<size_t>((static_cast<int>(index)/kVectorExtent + 1)*kVectorExtent));
	}

	v.resize(static_cast<size_t>(static_cast<int>(index) + 1));
}




#if 0
Model::ProtocolHandler::ProtocolHandler(const _ProtocolToken protocolToken,
		const _ProtocolHandlerId protocolHandlerId) noexcept :
	d(*new Private(protocolToken, protocolHandlerId))
{
}


Model::ProtocolHandler::~ProtocolHandler()
{
}


void Model::ProtocolHandler::add(const PacketHandler packetHandler) noexcept
{
	d->packetHandlers.push_back(packetHandler);
}
#endif




ModelPrivate::ModelPrivate(Model & model, const Info & info, Handler & handler) noexcept :
	model_(model),
	info_(info),
	handler_(handler),
	data_(info, std::bind(&ModelPrivate::_freePort, this, std::placeholders::_1))
{
}


ModelPrivate::~ModelPrivate()
{
#if 0
	for (Utils::IdGenerator::Iterator it(handlerManager_->d_->protocolIdGenerator_ ); it.hasNext(); )
	{
		ProtocolInfo & protocolInfo = protocolInfos_[ it.next() ];
		FLAT_ASSERT(!protocolInfo.isNull());
		protocolInfo.destroy( protocolInfo.protocolRef );
	}
	protocolInfos_.clear();
#endif
}


HandlerPrivate & ModelPrivate::handlerPrivate() noexcept
{
	return handler_.d();
}


const Storage & ModelPrivate::storage() const noexcept
{
	return handler_.d().storage();
}


#if 0
int ModelPrivate::protocolIdForMetaTypeId(const int protocolMetaTypeId) const
{
	const int protocolId = handlerManager_->d_->protocolIdForMetaTypeId(protocolMetaTypeId);
	FLAT_ASSERT(protocolId > 0);
	return protocolId;
}
#endif


void ModelPrivate::construct()
{
	isConstructingFromSnapshot_ = false;

	model_.construct();

	isConstructed_ = true;
}


void ModelPrivate::constructFromSnapshot(const Snapshot & snapshot)
{
#if 0
	// init snapshot
	const SnapshotPrivate * const snapshotPrivate = snapshot.d_;

	data_ = snapshotPrivate->data;

	// init validators
	{
		isConstructingFromSnapshot_ = true;

		{
			portValidator_ = Utils::IdGenerator( data_.portPool.limit() + data_.portPool.reserveCount() );
			for ( PoolIterator it( data_.portPool, Pool::IdMode_Taken ); it.hasNext(); )
				portValidator_.reserve( it.next() );
		}

		{
			idGeneratorValidator_ = Utils::IdGenerator( data_.idGeneratorsGenerator.idsLimit()
					+ data_.idGeneratorsGenerator.zombiesLimit() );
			idValidatorForGeneratorId_.resize( idGeneratorValidator_.limit() );
			for ( IdGeneratorIterator it( data_.idGeneratorsGenerator ); it.hasNext(); )
			{
				const SnapshotData::IdGeneratorInfo & idGeneratorInfo = data_.idGeneratorInfos.at( it.next() );

				idGeneratorValidator_.reserve( idGeneratorInfo.id );

				Utils::IdGenerator & idValidator = idValidatorForGeneratorId_[ idGeneratorInfo.id ];
				idValidator = Utils::IdGenerator( idGeneratorInfo.generator.idsLimit() + idGeneratorInfo.generator.zombiesLimit() );
				for ( IdGeneratorIterator idIt( idGeneratorInfo.generator ); idIt.hasNext(); )
					idValidator.reserve( idIt.next() );
			}
		}
	}

	// load handler first, this also constructs ports and id generators
	bool isConstructed = handler_->constructFromSnapshot( snapshotPrivate->handlerSnapshot );

	// process validators
	{
		isConstructingFromSnapshot_ = false;

		if ( isConstructed )
		{
			if ( portValidator_.count() != 0 )
			{
				isConstructed = false;
			}
			else if ( idGeneratorValidator_.count() != 0 )
			{
				isConstructed = false;
			}
			else
			{
				for ( IdGeneratorIterator it( data_.idGeneratorsGenerator ); it.hasNext(); )
				{
					if ( idValidatorForGeneratorId_.at( it.next() ).count() != 0 )
					{
						isConstructed = false;
						break;
					}
				}
			}
		}

		portValidator_ = Utils::IdGenerator();
		idGeneratorValidator_ = Utils::IdGenerator();
		idValidatorForGeneratorId_.clear();
	}

	if ( !isConstructed )
		return false;

	// emit created ports
	{
		for ( IdGeneratorIterator it( data_.portIdGenerator ); it.hasNext(); )
		{
			const SnapshotData::Port & port = data_.ports.at( it.next() );
			thread_->d_->portCreated( port.portId );
		}
	}

	isConstructed_ = true;
#endif
}


void ModelPrivate::setInput(Flat::Utils::Stream & input) noexcept
{
	FLAT_ASSERT(!input_);
	input_ = &input;
}


void ModelPrivate::resetInput() noexcept
{
	FLAT_ASSERT(input_);
	input_ = nullptr;
}


void ModelPrivate::processPacket()
{
	Utils::Stream::Reader reader = input_->lockForRead();

	try {
		_processPacket(*reader);
	} catch (const std::exception & e) {
		FLAT_ERROR << "Error processing packet:" << e;
		throw;
	}

	reader->commit();
}


void ModelPrivate::_processPacket(Flat::Utils::Stream::ReaderIf & reader)
{
	const auto type = reader.read<RouterProtocol::PacketType>();

	switch (type) {
	case RouterProtocol::PacketType::PeerCreated: {
		const auto p = reader.read<RouterProtocol::PeerCreatedPacket>();
		peerCreatedArrived(p.peerId);
	}
		break;

	case RouterProtocol::PacketType::PeerDestroyed: {
		const auto p = reader.read<RouterProtocol::PeerDestroyedPacket>();
		peerDestroyedArrived(p.peerId);
	}
		break;

	case RouterProtocol::PacketType::PeerPortPacket: {
		const auto p = reader.read<RouterProtocol::PeerPortPacket>();
		peerPortPacketArrived(p.peerId, p.portIndex, p.packetSize, reader);
	}
		break;

	case RouterProtocol::PacketType::PeerAdvanced: {
		const auto p = reader.read<RouterProtocol::PeerAdvancedPacket>();
		peerAdvancedArrived(p.peerId);
	}
		break;

	default:
		FLAT_FATAL << type;
	}
}


void ModelPrivate::watcherAdded(Watcher & watcher) noexcept
{
	model_.watcherAdded(watcher);
}


void ModelPrivate::watcherRemoved(Watcher & watcher) noexcept
{
	model_.watcherRemoved(watcher);
}


int ModelPrivate::availablePortCount() const noexcept
{
	return data_.portPool.availableCount();
}


int ModelPrivate::reservePortCount() const noexcept
{
	return data_.portPool.reserveCount();
}


PortId ModelPrivate::allocatePort(const ProtocolId protocolId,
		const Model::_ProtocolHandlerId handlerId, const int count) noexcept
{
	FLAT_ASSERT(!isConstructingFromSnapshot_);

	FLAT_ASSERT(data_.portPool.availableCount() > 0) << "Port limit reached";
	// TODO: Throw exception instead?
	FLAT_ASSERT(data_.portPool.reserveCount() > 0) << "Port is not available";

	const PortId::Index portIndex = static_cast<PortId::Index>(data_.portPool.take());
	FLAT_ASSERT(portIndex != PortId::Null);

	// save protocol id in data
	if (static_cast<int>(portIndex) >= static_cast<int>(data_.portInfos.size())) {
		data_.portInfos.resize(static_cast<size_t>(static_cast<int>(portIndex) + 1));
	}
	Data::PortInfo & dataPortInfo = data_.portInfos[static_cast<size_t>(portIndex)];
	FLAT_ASSERT(dataPortInfo.protocolId == ProtocolId::Null);
	dataPortInfo.protocolId = protocolId;

	_resizePortsTo(portIndex);

	PortInfo & portInfo = _portInfo(portIndex);
	FLAT_ASSERT(portInfo.isNull());

	portInfo.portIndex = portIndex;
	portInfo.protocolHandlerId = handlerId;
	portInfo.packetHandlers.resize(static_cast<size_t>(count));

	isAdvanced_ = true;

	handlerPrivate().portCreated(portIndex);

	return PortId(model_, portIndex);
}


void ModelPrivate::registerPortPacketHandler(const PortId::Index portIndex,
		const PacketId packetId, const Model::PacketHandler packetHandler) noexcept
{
	PortInfo & portInfo = _portInfo(portIndex);
	portInfo.packetHandlers[static_cast<size_t>(packetId)] = packetHandler;
}


void ModelPrivate::destroyPort(const PortId::Index portIndex) noexcept
{
	FLAT_ASSERT(!isConstructingFromSnapshot_);

	handlerPrivate().portDestroyed(portIndex);

	PortInfo & portInfo = _portInfo(portIndex);
	FLAT_ASSERT(!portInfo.isNull());

	portInfo = PortInfo();

	data_.portPool.free(static_cast<PortPool::Index>(portIndex));

	isAdvanced_ = true;
}


void ModelPrivate::_freePort(const PortPool::Index index) noexcept
{
}


int ModelPrivate::availableIdCount() const noexcept
{
	return data_.idPool.availableCount();
}


int ModelPrivate::reserveIdCount() const noexcept
{
	return data_.idPool.reserveCount();
}


Id ModelPrivate::takeId() noexcept
{
	FLAT_ASSERT(!isConstructingFromSnapshot_);

	FLAT_ASSERT(data_.idPool.availableCount() > 0) << "No free ids in pool";
	// TODO: Throw exception instead?
	FLAT_ASSERT(data_.idPool.reserveCount() > 0) << "No ids left";

	const Id::Index index = static_cast<Id::Index>(data_.idPool.take());
	FLAT_ASSERT(index != Id::Null);

	isAdvanced_ = true;

	return Id(model_, index);
}


void ModelPrivate::freeId(const Id::Index index) noexcept
{
	FLAT_ASSERT(!isConstructingFromSnapshot_);

	data_.idPool.free(index);

	isAdvanced_ = true;
}


#if 0
inline bool ModelPrivate::bindPort( const int portId, const int protocolId )
{
	FLAT_ASSERT(isConstructingFromSnapshot_);

	if ( portId <= 0 || portId > data_.ports.size() )
		return false;

	const SnapshotData::Port & port = data_.ports.at( portId );
	if ( port.isNull() )
		return false;

	if ( port.protocolId != protocolId )
		return false;

	if ( portValidator_.isFree( portId ) )
		return false;

	portValidator_.free( portId );

	return true;
}


inline bool ModelPrivate::bindIdGenerator( const int generatorId )
{
	FLAT_ASSERT(isConstructingFromSnapshot_);

	if ( generatorId <= 0 || generatorId > idGeneratorValidator_.limit() )
		return false;

	if ( idGeneratorValidator_.isFree( generatorId ) )
		return false;

	idGeneratorValidator_.free( generatorId );

	return true;
}


inline bool ModelPrivate::bindId( const int generatorId, const int id )
{
	FLAT_ASSERT(isConstructingFromSnapshot_);

	FLAT_ASSERT(generatorId > 0 && generatorId <= idGeneratorValidator_.limit());

	Utils::IdGenerator & idValidator = idValidatorForGeneratorId_[ generatorId ];
	FLAT_ASSERT(idValidator.limit() != -1);

	if ( id > idValidator.limit() )
		return false;

	if ( idValidator.isFree( id ) )
		return false;

	idValidator.free( id );

	return true;
}
#endif


#if 0
void ModelPrivate::readPacket(const PortId::Index portIndex, const int packetId, QDataStream & data) noexcept
{
	const Data::PortInfo & portInfo = data_.portInfo(portIndex);
	FLAT_ASSERT(!portInfo.isNull());

	// resolve port protocol and protocol handler
	const ModelPrivate::ProtocolInfo & protocolInfo = protocolInfos_.at( portInfo.protocolId );
	FLAT_ASSERT(!protocolInfo.isNull());

	protocolInfo.handlePacket( protocolInfo.protocolRef, packetId, portId, data, protocolInfo.protocolHandlerRef );
}
#endif


int ModelPrivate::delayForPeer(const int peerIndex) const noexcept
{
	const PeerId peerId = static_cast<PeerId>(peerIndex);
	FLAT_ASSERT(data_.peerPool.isValid(peerId));
	const int peerDelay = data_.peerPool.delayForPeer(peerId);
	FLAT_ASSERT(peerDelay <= delay());
	return peerDelay;
}


void ModelPrivate::advance()
{
	FLAT_ASSERT(delay() < info_.delayLimit());

	data_.portPool.advance();
	data_.idPool.advance();
	data_.peerPool.advance();
}


void ModelPrivate::peerCreated(const PeerId peerId) noexcept
{
	data_.peerPool.create(peerId);
}


void ModelPrivate::peerDestroyed(const PeerId peerId) noexcept
{
	data_.portPool.peerRemoved(peerId);
	data_.idPool.peerRemoved(peerId);
	data_.peerPool.destroy(peerId);
}


void ModelPrivate::peerAdvanced(const PeerId peerId) noexcept
{
	data_.portPool.advancePeer(peerId);
	data_.idPool.advancePeer(peerId);
	data_.peerPool.advancePeer(peerId);
}


#if 0
Snapshot ModelPrivate::makeSnapshot() const
{
	SnapshotPrivate * const snapshotPrivate = new SnapshotPrivate(data_, handler_.snapshot());
	return Snapshot(snapshotPrivate);
}
#endif


int ModelPrivate::delay() const noexcept
{
	return data_.peerPool.delay();
}


void ModelPrivate::peerCreatedArrived(const PeerId peerId)
{
	// check the peerId is in allowed range
	if (static_cast<int>(peerId) < 0 || static_cast<int>(peerId) >= info_.peerLimit()) {
		throw Error();
	}

	if (data_.peerPool.isValid(peerId)) {
		throw Error();
	}

	peerCreated(peerId);

	// inform handler
	handlerPrivate().peerCreated(peerId);
}


void ModelPrivate::peerDestroyedArrived(const PeerId peerId)
{
	// check the peerId is in allowed range
	if (static_cast<int>(peerId) < 0 || static_cast<int>(peerId) >= info_.peerLimit()) {
		throw Error();
	}

	// check that peer exists
	if (!data_.peerPool.isValid(peerId)) {
		throw Error();
	}

	// inform handler
	handlerPrivate().peerDestroyed(peerId);

	peerDestroyed(peerId);
}


void ModelPrivate::peerPortPacketArrived(const PeerId peerId, const PortId::Index portIndex,
		const int packetSize, Utils::Stream::ReaderIf & reader)
{
	// check that delay is not reached limit
	if (data_.peerPool.delay() == info_.delayLimit()) {
		throw RouterError();
	}

	// check the peerId is in allowed range
	if (static_cast<int>(peerId) < 0 || static_cast<int>(peerId) >= info_.peerLimit()) {
		throw RouterError();
	}

	// check that peer exists
	if (!data_.peerPool.isValid(peerId)) {
		throw RouterError();
	}

	if (static_cast<int>(portIndex) < 0 || static_cast<int>(portIndex) >= info_.portMax()
			|| !data_.portPool.validate(peerId, portIndex)) {
		throw PeerError(peerId);
	}

	const ProtocolId protocolId = data_.portInfoAt(portIndex).protocolId;

	const int protocolPacketCount = static_cast<int>(
			info_.d().protocolInfo(protocolId).packets.size());

	const auto packetId = reader.read<PacketId>();
	if (static_cast<int>(packetId) < 0 || static_cast<int>(packetId) >= protocolPacketCount) {
		throw PeerError(peerId);
	}

	const Info::Private::PacketInfo & packetInfo = info_.d().packetInfo(protocolId, packetId);

	const int dataSize = packetSize - sizeof(uint32_t); // packetId

	if (dataSize < packetInfo.size.min || dataSize > packetInfo.size.max) {
		throw PeerError(peerId);
	}

	if (reader.bytesAvailable() < dataSize) {
		throw Utils::Stream::IncompleteError{};
	}

	const PortInfo & portInfo = _portInfo(portIndex);

	const Model::PacketHandler & handler = portInfo.packetHandlers.at(static_cast<size_t>(packetId));

	const int streamPos = reader.pos();

	handler(portInfo.protocolHandlerId, reader, packetInfo.reader);

	const int readDataSize = reader.pos() - streamPos;
	if (readDataSize != dataSize) {
		throw PeerError(peerId);
	}

#if 0
	// check the packet size
	if ( packetSize > handlerManager_->d_->maxProtocolPacketSize_ )
	{
		_emitRouterCorrupted();
		return false;
	}

	// check the portId is in allowed range
	bool isPortOk = false;

	if ( !peerInfo.isCorrupted )
	{
		if ( portId <= 0 || portId > handlerManager_->portLimit() )
			peerInfo.isCorrupted = true;
	}

	if ( !peerInfo.isCorrupted )
	{
		const Pool::ValidationResult portValidationResult = handler_->d_->data_.portPool.validate( peerId, portId );
		switch ( portValidationResult )
		{
		case Pool::ValidationResult_Taken:
			isPortOk = true;
			break;

		case Pool::ValidationResult_Zombie:
			// normal zombie port, ignore packet, but peer is ok
			break;

		default:
			FLAT_WARNING << "Port validation failed:" << portValidationResult;
			peerInfo.isCorrupted = true;
		}
	}

	// At this stage only peer might be corrupted, not router.
	// In that case packet size is in allowed range and can be skipped without interrupting the whole stream processing.
	// Router master is responsible to kick the broken peer as soon as possible, but even without that it is ok to
	// continue processing other peer packets.

	if ( ds.device()->bytesAvailable() < packetSize )
	{
		ds.setStatus( QDataStream::ReadPastEnd );
		return true;
	}

	if ( peerInfo.isCorrupted )
	{
		ds.skipRawData( packetSize );
		return true;
	}

	// 8 == packetId (qint32) + packetSize (qint32)
	if ( packetSize < 8 )
	{
		// peer protocol violation
		peerInfo.isCorrupted = true;
		ds.skipRawData( packetSize );
		return true;
	}

	// skip packet without protocol checking if port is zombie
	if ( !isPortOk )
	{
		ds.skipRawData( packetSize );
		return true;
	}

	const int expectedPortPacketSize = packetSize - 8;

	handlerPeerId_ = peerId;
	handlerPortId_ = portId;
	handlerPacketId_ = dsr.readInt32();
	handlerPacketSize_ = dsr.readInt32();

	const SnapshotData::PortInfo & portInfo = handler_->d_->data_.portInfos.at( portId );
	FLAT_ASSERT(!portInfo.isNull());

	const HandlerManagerPrivate::ProtocolInfo & protocolInfo = handlerManager_->d_->protocolInfoForId_.at( portInfo.protocolId );
	FLAT_ASSERT(!protocolInfo.isNull());

	if ( handlerPacketId_ < 0 || handlerPacketId_ >= protocolInfo.packetCount )
	{
		// invalid packet id
		peerInfo.isCorrupted = true;
		ds.skipRawData( expectedPortPacketSize );
		return true;
	}

	if ( handlerPacketSize_ > protocolInfo.maxSizeForPacket.at( handlerPacketId_ ) )
	{
		// invalid packet size
		peerInfo.isCorrupted = true;
		ds.skipRawData( expectedPortPacketSize );
		return true;
	}

	handlerDataStream_.setDevice( ds.device() );

	if ( handlerPacketSize_ != expectedPortPacketSize )
	{
		peerInfo.isCorrupted = true;
		ds.skipRawData( expectedPortPacketSize );
		return true;
	}

	if ( packetProcessingPolicy_ == Thread::PacketProcessing_Synchronous )
	{
		// process this packet inlined
		_startHandlerPacket();
		_processHandlerPacket();
		_finishHandlerPacket();
	}
	else
	{
		_processHandler();
	}

	return true;
#endif
}


void ModelPrivate::peerAdvancedArrived(const PeerId peerId)
{
	// check the peerId is in allowed range
	if (static_cast<int>(peerId) < 0 || static_cast<int>(peerId) >= info_.peerLimit()) {
		throw Error();
	}

	// check that peer exists
	if (!data_.peerPool.isValid(peerId)) {
		throw Error();
	}

	if (data_.peerPool.delayForPeer(peerId) == info_.delayLimit()) {
		throw Error();
	}

#if 0
	SnapshotData::PeerInfo & peerInfo = handler_->d_->data_.peerInfos[ peerId ];
	if ( peerInfo.isNull() )
	{
		_emitRouterCorrupted();
		return false;
	}

	if ( peerInfo.isCorrupted )
		return true;

	if ( peerInfo.delay == handler_->d_->delay() )
	{
		peerInfo.isCorrupted = true;
		_emitPeerCorrupted( peerId );
		return true;
	}

	handler_->d_->peerAdvanced( peerId );
#endif
}


void ModelPrivate::_resizePortsTo(const PortId::Index index) noexcept
{
	reserveVectorIndex(portInfos_, index);
}




}}}
