
#pragma once




namespace Flat { namespace Game { namespace Model {




class Model;
class ModelPrivate;




template <typename T>
class PoolId
{
public:
	enum class Index { Null = -1 };
	static constexpr Index Null = Index::Null;

	PoolId() noexcept;
	PoolId(PoolId && id) noexcept;
	PoolId(const PoolId & id) = delete;
	PoolId & operator=(const PoolId & id) = delete;
	PoolId & operator=(PoolId && id) noexcept;
	~PoolId();

	bool isNull() const noexcept;

	Index value() const noexcept;
	int index() const noexcept;

	void free() noexcept;

	operator bool() const noexcept;

private:
	PoolId(Model & model, Index index) noexcept;

private:
	Model * model_;
	Index index_ = Null;

	friend class ModelPrivate;
};




template <typename T>
inline PoolId<T>::PoolId() noexcept
{}

template <typename T>
inline PoolId<T>::PoolId(PoolId && id) noexcept :
	model_(id.model_),
	index_(id.index_)
{ id.index_ = Null; }

template <typename T>
inline PoolId<T> & PoolId<T>::operator=(PoolId && id) noexcept
{ model_ = id.model_; index_ = id.index_; id.index_ = Null; return *this; }

template <typename T>
inline bool PoolId<T>::isNull() const noexcept
{ return index_ == Null; }

template <typename T>
inline typename PoolId<T>::Index PoolId<T>::value() const noexcept
{ return index_; }

template <typename T>
inline int PoolId<T>::index() const noexcept
{ return static_cast<int>(index_); }

template <typename T>
inline PoolId<T>::PoolId(Model & model, const Index index) noexcept :
	model_(&model),
	index_(index)
{}

template <typename T>
inline PoolId<T>::~PoolId()
{ if (!isNull()) free(); }

template <typename T>
inline PoolId<T>::operator bool() const noexcept
{ return !isNull(); }




}}}
