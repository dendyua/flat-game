
#pragma once

#include <memory>
#include <functional>

#include <Flat/Core/Ref.hpp>
#include <Flat/Utils/Stream.hpp>

#include "Debug.hpp"
#include "Protocol.hpp"
#include "WatcherType.hpp"




namespace Flat { namespace Game {
	class Handler;
}}

namespace Flat { namespace Game { namespace Model {




class Watcher;
class Model;




class Context
{
public:
	virtual ~Context() = default;
};




class FLAT_GAME_EXPORT Info
{
public:
	static constexpr int kInvalidVersion = -1;

	Info() noexcept;
	~Info();

	int peerLimit() const noexcept;
	void setupPeers(int limit) noexcept;

	int portLimit() const noexcept;
	int portReserve() const noexcept;
	int portMax() const noexcept;
	void setupPorts(int limit, int reserve) noexcept;

	int poolLimit() const noexcept;
	int poolReserve() const noexcept;
	int poolMax() const noexcept;
	void setupPool(int limit, int reserve) noexcept;

	int delayLimit() const noexcept;
	void setupDelay(int limit) noexcept;

	template <typename M>
	void registerModel() noexcept;

	template <typename W>
	void registerWatcher() noexcept;

	template <typename M>
	void registerProtocols() noexcept;

	std::unique_ptr<Model> createModel(Handler & handler) const noexcept;

	template <typename W>
	bool isWatcherSupported() const noexcept;

	std::unique_ptr<Watcher> createWatcher(WatcherType watcherType) const noexcept;

	class Private;
	const Private & d() const noexcept;

	typedef std::function<Model*(Handler &)> ModelAllocator;
	typedef std::function<Watcher*()> WatcherAllocator;

	typedef std::function<void(Utils::Stream::WriterIf&, const _PacketBase&)> PacketWriter;
	typedef std::function<void(Utils::Stream::ReaderIf&, _PacketBase&)> PacketReader;

private:
	template <bool Empty, int Index, typename List> struct ProtocolRegistrator;
	template <bool Empty, int Index, typename List> struct PacketRegistrator;

private:
	std::unique_ptr<Context> _context(Handler & handler) const noexcept;
	void _registerModel(ModelAllocator allocator) noexcept;
	void _registerWatcher(WatcherType watcherType, WatcherAllocator allocator) noexcept;
	void _allocateProtocols(int count) noexcept;
	void _allocateProtocolPackets(ProtocolId protocolId, int count) noexcept;
	void _registerProtocolPacket(ProtocolId protocolId, PacketId packetId, PacketReader reader,
			PacketWriter writer, PacketSize size) noexcept;
//	void _registerProtocol(ProtocolDescriptor & protocolDescriptor) noexcept;
	bool _isWatcherSupported(WatcherType watcherType) noexcept;

private:
	Ref<Private> d_;
};




inline int Info::portMax() const noexcept
{ return portLimit() + portReserve(); }

inline int Info::poolMax() const noexcept
{ return poolLimit() + poolReserve(); }

template <typename M>
inline void Info::registerModel() noexcept
{
	static_assert(std::is_base_of<Model, M>::value, "Must be derived from Model");
	_registerModel([this](Handler & handler){return new M(*_context(handler));});
}

template <typename W>
inline void Info::registerWatcher() noexcept
{
	static_assert(std::is_base_of<Watcher, W>::value, "Must be derived from Watcher");
	_registerWatcher(W::staticWatcherType(), [](){ return new W; });
}

template <int Index, typename... Types>
struct Info::PacketRegistrator<true, Index, std::tuple<Types...>>
{
	static void exec(Info &, ProtocolId) {}
};

template <typename T, int Index, typename... Types>
struct Info::PacketRegistrator<false, Index, std::tuple<T, Types...>>
{
	static void exec(Info & info, const ProtocolId protocolId)
	{
		typedef T Packet;

		constexpr PacketId packetId = static_cast<PacketId>(Index);

		const Info::PacketWriter writer = [] (Utils::Stream::WriterIf & w, const _PacketBase & p) {
			const Packet & packet = static_cast<const Packet&>(p);
			_PacketSerializer<Packet, Packet::kSerializationMode>::write(w, packet);
		};

		const Info::PacketReader reader = [] (Utils::Stream::ReaderIf & r, _PacketBase & p) {
			Packet & packet = static_cast<Packet&>(p);
			_PacketSerializer<Packet, Packet::kSerializationMode>::read(r, packet);
		};

		constexpr PacketSize size = _PacketSize<Packet>::size;
		info._registerProtocolPacket(protocolId, packetId, reader, writer, size);

		PacketRegistrator<sizeof...(Types) == 0, Index + 1, std::tuple<Types...>>::exec(info, protocolId);
	}
};

template <int Index, typename... Types>
struct Info::ProtocolRegistrator<true, Index, std::tuple<Types...>>
{
	static void exec(Info &) {}
};

template <typename T, int Index, typename... Types>
struct Info::ProtocolRegistrator<false, Index, std::tuple<T, Types...>>
{
	static void exec(Info & info)
	{
		typedef T Protocol;
		typedef typename ProtocolPacketsInfo<Protocol>::Packets Packets;

		constexpr ProtocolId protocolId = static_cast<ProtocolId>(Index);

		info._allocateProtocolPackets(protocolId, std::tuple_size<Packets>::value);

		PacketRegistrator<std::tuple_size<Packets>::value == 0, 0, Packets>::exec(info, protocolId);

		ProtocolRegistrator<sizeof...(Types) == 0, Index + 1, std::tuple<Types...>>::exec(info);
	}
};

template <typename M>
inline void Info::registerProtocols() noexcept
{
	typedef typename ModelProtocolsInfo<M>::Protocols Protocols;

	_allocateProtocols(std::tuple_size<Protocols>::value);

	ProtocolRegistrator<std::tuple_size<Protocols>::value == 0, 0, Protocols>::exec(*this);
}

template <typename W>
inline bool Info::isWatcherSupported() const noexcept
{
	static_assert(std::is_base_of<Watcher, W>::value, "Must be derived from Watcher");
	return _isWatcherSupported(WatcherTraits<W>::type());
}

inline const Info::Private & Info::d() const noexcept
{ return *d_; }




}}}
