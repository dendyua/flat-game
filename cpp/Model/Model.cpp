
#include "Model.hpp"
#include "Model_p.hpp"

#include "Snapshot.hpp"
#include "RouterProtocol_p.hpp"
#include "Context_p.hpp"




namespace Flat { namespace Game { namespace Model {




Model::Model(const Context & context) noexcept :
	d_(*new ModelPrivate(*this,
			static_cast<const ContextPrivate&>(context).info,
			static_cast<const ContextPrivate&>(context).handler))
{
}


Model::~Model()
{
}


const Storage & Model::storage() const noexcept
{
	return d_->storage();
}


bool Model::isConstructed() const noexcept
{
	return d_->isConstructed();
}


std::any Model::snapshot() const
{
	return {};
}


bool Model::constructFromSnapshot(const std::any & snapshot)
{
	FLAT_UNUSED(snapshot)
	return false;
}


#if 0
int Model::_protocolIdForMetaTypeId(const int protocolMetaTypeId) const
{
	return d_->protocolIdForMetaTypeId(protocolMetaTypeId);
}
#endif


#if 0
void Model::_registerProtocolHandler(const int protocolId, ProtocolHandlerRef * const protocolHandlerRef,
		const ProtocolCreateFunction create,
		const ProtocolDestroyFunction destroy,
		const ProtocolHandlePacketFunction handlePacket)
{
	d_->registerProtocolHandler(protocolId, protocolHandlerRef, create, destroy, handlePacket);
}
#endif


int Model::availablePortCount() const noexcept
{
	return d_->availablePortCount();
}


int Model::reservePortCount() const noexcept
{
	return d_->reservePortCount();
}


PortId Model::_allocatePort(const ProtocolId protocolId, const _ProtocolHandlerId handlerId,
		const int count) noexcept
{
	return d_->allocatePort(protocolId, handlerId, count);
}


void Model::_registerPortPacketHandler(const PortId::Index portIndex, const PacketId packetId,
		const PacketHandler handler) noexcept
{
	d_->registerPortPacketHandler(portIndex, packetId, handler);
}


void Model::_freePortId(const PortId::Index index) noexcept
{
	d_->destroyPort(index);
}


void Model::advance()
{
	d_->markAdvanced();
}


int Model::availableIdCount() const noexcept
{
	return d_->availableIdCount();
}


Id Model::takeId() noexcept
{
	return d_->takeId();
}


void Model::_freeId(const Id::Index index) noexcept
{
	d_->freeId(index);
}


Correction & Model::correction()
{
	return d_->correction();
}


#if 0
bool Model::_bindPort( const int portId, const int protocolId )
{
	return d_->bindPort( portId, protocolId );
}


bool Model::bindIdGenerator( const int generatorId )
{
	return d_->bindIdGenerator( generatorId );
}


bool Model::bindId( const int generatorId, const int id )
{
	return d_->bindId( generatorId, id );
}
#endif




}}}
