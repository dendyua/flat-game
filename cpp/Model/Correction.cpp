
#include "Correction.hpp"

#include <boost/crc.hpp>




namespace Flat { namespace Game { namespace Model {




class CorrectionStream : public Flat::Utils::Stream
{
public:
	CorrectionStream(CorrectionPrivate & correctionPrivate);

	int size() const override { return -1; }
	int pos() const override { return -1; }
	void seek(int pos) override {}
	int bytesAvailable() const override { return -1; }
	int readData(uint8_t * data, int size) override { return -1; }
	void commit(int pos) override {}
	int writeData(const uint8_t * data, int size) override;

private:
	CorrectionPrivate & correctionPrivate_;
};




class CorrectionPrivate
{
public:
	inline static constexpr int kHashSize = 20;
	static_assert((kHashSize % sizeof(Correction::Result)) == 0);

	CorrectionPrivate() noexcept;

	void clear() noexcept;
	void write(const uint8_t * data, int size) noexcept;

	Flat::Utils::Stream::Writer stream() noexcept;

	Correction::Result finish() noexcept;

private:
	CorrectionStream stream_;
	boost::crc_basic<32> crc_;
};




CorrectionStream::CorrectionStream(CorrectionPrivate & correctionPrivate) :
	correctionPrivate_(correctionPrivate)
{
}


int CorrectionStream::writeData(const uint8_t * const data, const int size)
{
	correctionPrivate_.write(data, size);
	return size;
}




inline CorrectionPrivate::CorrectionPrivate() noexcept :
	stream_(*this),
	crc_(0x1021)
{
}


inline void CorrectionPrivate::write(const uint8_t * const data, const int size) noexcept
{
	crc_.process_bytes(data, size);
}


inline Flat::Utils::Stream::Writer CorrectionPrivate::stream() noexcept
{
	return stream_.lockForWrite();
}


inline Correction::Result CorrectionPrivate::finish() noexcept
{
	return crc_.checksum();
}




Correction::Correction() :
	d_(*new CorrectionPrivate)
{
}


Correction::~Correction()
{
}


Flat::Utils::Stream::Writer Correction::stream()
{
	return d_->stream();
}


Correction::Result Correction::finish()
{
	return d_->finish();
}




}}}
