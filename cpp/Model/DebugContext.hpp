
#pragma once

#include <Flat/Debug/Debug.hpp>

FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(FlatGameModel, FLAT_GAME_SOURCE_DIR "/cpp/Model", true)
FLAT_DEBUG_DECLARE_SECONDARY_DEBUG_CONTEXT(FlatGameModel, 1, FLAT_GAME_SOURCE_DIR "/include/Flat/Game/Model")
