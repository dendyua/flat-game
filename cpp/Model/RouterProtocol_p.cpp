
#include "RouterProtocol_p.hpp"




namespace Flat { namespace Utils {

template <> void Stream::Serializer<Game::Model::RouterProtocol::PacketType>::write(
		Stream::WriterIf & w, const Game::Model::RouterProtocol::PacketType & p) {
	w.write(uint32_t(p));
}

template <> void Stream::Serializer<Game::Model::RouterProtocol::PacketType>::read(
		Stream::ReaderIf & r, Game::Model::RouterProtocol::PacketType & p) {
	p = Game::Model::RouterProtocol::PacketType(r.read<uint32_t>());

	if (FLAT_UNLIKELY(p < Game::Model::RouterProtocol::PacketType::PeerCreated ||
			p >= Game::Model::RouterProtocol::PacketType::Max)) {
		throw IncompleteError{};
	}
}


template <> void Stream::Serializer<Game::Model::RouterProtocol::PeerCreatedPacket>::write(
		Stream::WriterIf & w, const Game::Model::RouterProtocol::PeerCreatedPacket & p) {
	w.write(p.peerId);
}

template <> void Stream::Serializer<Game::Model::RouterProtocol::PeerCreatedPacket>::read(
		Stream::ReaderIf & r, Game::Model::RouterProtocol::PeerCreatedPacket & p) {
	r.read(p.peerId);
}


template <> void Stream::Serializer<Game::Model::RouterProtocol::PeerDestroyedPacket>::write(
		Stream::WriterIf & w, const Game::Model::RouterProtocol::PeerDestroyedPacket & p) {
	w.write(p.peerId);
}

template <> void Stream::Serializer<Game::Model::RouterProtocol::PeerDestroyedPacket>::read(
		Stream::ReaderIf & r, Game::Model::RouterProtocol::PeerDestroyedPacket & p) {
	r.read(p.peerId);
}


template <> void Stream::Serializer<Game::Model::RouterProtocol::PeerPortPacket>::write(
		Stream::WriterIf & w, const Game::Model::RouterProtocol::PeerPortPacket & p) {
	w.write(p.peerId);
	w.write(p.portIndex);
	w.write<int32_t>(p.packetSize);
}

template <> void Stream::Serializer<Game::Model::RouterProtocol::PeerPortPacket>::read(
		Stream::ReaderIf & r, Game::Model::RouterProtocol::PeerPortPacket & p) {
	r.read(p.peerId);
	r.read(p.portIndex);
	p.packetSize = r.read<int32_t>();
}


template <> void Stream::Serializer<Game::Model::RouterProtocol::PeerAdvancedPacket>::write(
		Stream::WriterIf & w, const Game::Model::RouterProtocol::PeerAdvancedPacket & p) {
	w.write(p.peerId);
}

template <> void Stream::Serializer<Game::Model::RouterProtocol::PeerAdvancedPacket>::read(
		Stream::ReaderIf & r, Game::Model::RouterProtocol::PeerAdvancedPacket & p) {
	r.read(p.peerId);
}

}}
