
#pragma once

#include <istream>
#include <memory>
#include <stdexcept>




namespace Flat {
namespace Game {
namespace Model {




class FLAT_GAME_EXPORT Storage
{
public:
	class Error : public std::runtime_error
	{
	public:
		using std::runtime_error::runtime_error;
	};

	struct Iterator {
		virtual ~Iterator() = 0;
		virtual bool equals(const Iterator & other) const = 0;
		virtual void advance() = 0;
	};

	class Directory
	{
	public:
		virtual ~Directory() = default;
		virtual std::unique_ptr<Iterator> begin() const = 0;
		virtual std::unique_ptr<Iterator> end() const = 0;
	};

	struct Dir {
		struct It {
			std::unique_ptr<Iterator> p;
			void operator++() { p->advance(); }
			bool operator==(const It & other) const { return other.p->equals(*p); }
		};

		It begin() const;
		It end() const;
	};

	virtual ~Storage() = default;
	virtual std::unique_ptr<Directory> directory(const char * path) = 0;
	virtual std::unique_ptr<std::istream> open(const char * filePath) const = 0;
};




}}}
