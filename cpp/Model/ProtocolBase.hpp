
#pragma once

#include <stdexcept>
#include <functional>
#include <string>

#include <Flat/Core/Ref.hpp>
#include <Flat/Utils/Stream.hpp>




namespace Flat { namespace Game { namespace Model {




enum class ProtocolId { Null = -1 };
enum class PacketId { Null = -1 };




class ProtocolBase {};

template <typename M>
class Protocol : public ProtocolBase
{
public:
	typedef M Model;
};

template <typename M> struct ModelProtocolsInfo;

template <typename P> struct ProtocolPacketsInfo;




template <typename T, typename Tuple> struct _TupleIndexResolver;

template <typename T, typename... Types>
struct _TupleIndexResolver<T, std::tuple<T, Types...>>
{
	static constexpr int value = 0;
};

template <typename T, typename U, typename... Types>
struct _TupleIndexResolver<T, std::tuple<U, Types...>>
{
	static constexpr int value = 1 + _TupleIndexResolver<T, std::tuple<Types...>>::value;
};

template <typename P>
inline constexpr ProtocolId _protocolId()
{
	typedef typename P::Model Model;
	typedef typename ModelProtocolsInfo<Model>::Protocols Protocols;

	return static_cast<ProtocolId>(_TupleIndexResolver<P, Protocols>::value);
}

template <typename P>
inline constexpr PacketId _packetId()
{
	typedef typename P::Protocol Protocol;
	typedef typename ProtocolPacketsInfo<Protocol>::Packets Packets;

	return static_cast<PacketId>(_TupleIndexResolver<P, Packets>::value);
}




enum class _PacketSerializationMode
{
	Void,  // packet has no fields, no serialization
	Tuple, // packet fields specified in Tuple, automatic serialization
	Custom // packet has custom serialization in Serializer's read and write specializations
};




// base class exists only to static assert that packet is derived from it
class _PacketBase {};




class SerializationError : public std::runtime_error
{
public:
	SerializationError(const std::string & message = std::string()) :
		std::runtime_error(message)
	{}
};




}}}




FLAT_UTILS_STREAM_DECLARE_ENUM(Game::Model::PacketId, uint32_t)
