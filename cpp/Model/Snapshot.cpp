
#include "Snapshot.hpp"

#include "Model_p.hpp"




namespace Flat { namespace Game { namespace Model {




Snapshot::Snapshot()
{
}


Snapshot::Snapshot(const Snapshot & snapshot) :
	d_(snapshot.d_)
{
}


Snapshot::Snapshot(SnapshotPrivate * const d) :
	d_(d)
{
}


Snapshot & Snapshot::operator=(const Snapshot & snapshot)
{
	d_ = snapshot.d_;
	return *this;
}


Snapshot::~Snapshot()
{
}


bool Snapshot::isNull() const
{
	return !d_;
}




#if 0
void ModelPrivate::initSnapshotDataStream( QDataStream & ds )
{
	// TODO: Change big endian to little?
	ds.setByteOrder( QDataStream::BigEndian );
	ds.setVersion(  );
}


void HandlerPrivate::_writeSnapshotDataHeader( QDataStream & ds, const SnapshotData & s,
		const HandlerManagerPrivate * const handlerManagerPrivate )
{
#if 0
	// save transmitters
	{
		ds << (quint32)s.transmitterIds.count();
		for ( QListIterator<int> it( s.transmitterIds ); it.hasNext(); )
		{
			const SnapshotData::Transmitter & transmitter = s.transmitters.at( it.next() );
			FLAT_ASSERT(!transmitter.isNull());

			ds << (quint32)transmitter.transmitterId;

			ds << (quint32)transmitter.holdDelayLimit;
			ds << (quint32)transmitter.resumeDelayLimit;

			ds << (quint32)transmitter.delay;
			ds << (quint32)transmitter.hitCount;

			ds << (quint32)transmitter.history.count();
			for ( QListIterator<SnapshotData::Transmitter::State> historyIt( transmitter.history ); historyIt.hasNext(); )
			{
				const SnapshotData::Transmitter::State & transmitterState = historyIt.next();
				ds << quint8( transmitterState.transmittersOnHold ? 1 : 0);
			}
		}
	}

	// save ports
	{
		ds << s.portIdGenerator;

		for ( IdGeneratorIterator it( s.portIdGenerator ); it.hasNext(); )
		{
			const SnapshotData::Port & port = s.ports.at( it.next() );
			FLAT_ASSERT(!port.isNull());

			ds << (quint32)port.protocolId;
			ds << (quint32)port.bindedTransmitter;
		}

		for ( IdGeneratorZombieIterator zit( s.portIdGenerator ); zit.hasNext(); )
		{
			const SnapshotData::Port & port = s.ports.at( zit.next() );
			FLAT_ASSERT(!port.isNull());

			ds << (quint32)port.protocolId;
		}
	}

	// save id generators
	{
		ds << s.idGeneratorsGenerator;

		for ( IdGeneratorIterator it( s.idGeneratorsGenerator ); it.hasNext(); )
		{
			const SnapshotData::IdGeneratorInfo & idGeneratorInfo = s.idGeneratorInfos.at( it.next() );

			ds << quint32( idGeneratorInfo.type );
			ds << idGeneratorInfo.generator;
		}
	}

	// save on hold flag
	ds << quint8( s.transmittersOnHold ? 1 : 0 );
#endif
}


void HandlerPrivate::_writeSnapshotDataPendingPackets( QDataStream & ds, const SnapshotData & s,
		const HandlerManagerPrivate * const handlerManagerPrivate )
{
#if 0
	// save pending packets
	ds << quint32( s.pendingPackets.count() );
	for ( QListIterator<SnapshotData::Packet> it( s.pendingPackets ); it.hasNext(); )
	{
		const SnapshotData::Packet & packet = it.next();

		ds << quint32( packet.portId );

		const SnapshotData::Port & port = s.ports.at( packet.portId );
		FLAT_ASSERT(!port.isNull());

		handlerManagerPrivate->takeProtocolForId( port.protocolId )->writePacket( ds, packet.packet );
	}
#else
	FLAT_FATAL;
#endif
}


void HandlerPrivate::writeSnapshot( QDataStream & ds, const Snapshot & s, const StreamPrivate * const threadPrivate )
{
#if 0
	QBuffer buffer;
	buffer.open( QIODevice::WriteOnly );

	QDataStream bufferDs( &buffer );
	initSnapshotDataStream( bufferDs );

	// dummy to reserve space for the snapshot size
	bufferDs << quint32( 0 );

	_writeSnapshotDataHeader( bufferDs, s.d_->data, threadPrivate );
	_writeSnapshotDataPendingPackets( bufferDs, s.d_->data, threadPrivate );
	threadPrivate->handlerManager()->writeSnapshot( bufferDs, s.d_->handlerSnapshot );

	// write actual snapshot size
	const int snapshotSize = buffer.pos() - sizeof(quint32);
	buffer.seek( 0 );
	bufferDs << quint32( snapshotSize );

	buffer.close();

	initSnapshotDataStream( ds );
	ds.writeRawData( buffer.data().constData(), buffer.data().size() );
#else
	FLAT_FATAL;
#endif
}


bool HandlerPrivate::_readSnapshotDataHeaderInternal( QDataStream & ds, SnapshotData & s,
		const StreamPrivate * const threadPrivate )
{
#if 0
	if ( ds.status() != QDataStream::Ok )
		return true;

	SnapshotData data;

	const int peerLimit = threadPrivate->handlerManager()->peerLimit();
	const int delayLimit = threadPrivate->handlerManager()->delayLimit();
	const int portLimit = threadPrivate->handlerManager()->portLimit();

	const int totalIdGeneratorsLimit = threadPrivate->handlerManager()->d_->totalIdGeneratorsLimit();
	const int idGeneratorTypesCount = threadPrivate->handlerManager()->idGeneratorTypesCount();

	// read transmitters
	{
		const int transmittersCount = dsr.readUint32();
		if ( ds.status() != QDataStream::Ok )
			return true;

		if ( transmittersCount < 0 || transmittersCount > transmittersLimit )
			return false;

		for ( int i = 0; i < transmittersCount; ++i )
		{
			const int transmitterId = dsr.readUint32();
			if ( ds.status() != QDataStream::Ok )
				return true;

			if ( transmitterId <= 0 || transmitterId > transmittersLimit )
				return false;

			data.resizeTransmittersTo( transmitterId, transmittersLimit );

			SnapshotData::Transmitter & transmitter = data.transmitters[ transmitterId ];
			if ( !transmitter.isNull() )
				return false;

			transmitter.transmitterId = transmitterId;

			transmitter.holdDelayLimit = dsr.readUint32();
			transmitter.resumeDelayLimit = dsr.readUint32();
			if ( ds.status() != QDataStream::Ok )
				return true;

			if ( transmitter.holdDelayLimit < 0 || transmitter.holdDelayLimit > transmittersDelayLimit )
				return false;

			if ( transmitter.resumeDelayLimit < 0 || transmitter.resumeDelayLimit > transmittersDelayLimit )
				return false;

			if ( transmitter.holdDelayLimit < transmitter.resumeDelayLimit )
				return false;

			transmitter.delay = dsr.readUint32();
			transmitter.hitCount = dsr.readUint32();
			if ( ds.status() != QDataStream::Ok )
				return true;

			if ( transmitter.delay < 0 || transmitter.hitCount < 0 || transmitter.delay < transmitter.hitCount )
				return false;

			const int historyCount = dsr.readUint32();
			if ( ds.status() != QDataStream::Ok )
				return true;

			if ( historyCount != transmitter.delay - transmitter.hitCount + 1 )
				return false;

			for ( int j = 0; j < historyCount; j++ )
			{
				const quint8 transmittersOnHold = dsr.readUint8();
				if ( ds.status() != QDataStream::Ok )
					return true;

				if ( transmittersOnHold != 0 && transmittersOnHold != 1 )
					return false;

				SnapshotData::Transmitter::State transmitterState;
				transmitterState.transmittersOnHold = transmittersOnHold;
				transmitter.history << transmitterState;
			}

			data.transmitterIds << transmitter.transmitterId;
		}
	}

	// read ports
	{
		data.portIdGenerator.setIdsLimit( portsLimit );
		data.portIdGenerator.setTransmittersLimit( transmittersLimit );
		data.portIdGenerator.setZombiesLimit( _zombiesLimit( transmittersDelayLimit, portsLimit ) );

		ds >> data.portIdGenerator;
		if ( ds.status() != QDataStream::Ok )
			return true;

		for ( IdGeneratorIterator portsIt( data.portIdGenerator ); portsIt.hasNext(); )
		{
			const int portId = portsIt.next();

			data.resizePortsTo( portId, portsLimit );

			SnapshotData::Port & port = data.ports[ portId ];
			if ( !port.isNull() )
				return false;

			port.portId = portId;
			port.protocolId = dsr.readUint32();
			port.bindedTransmitter = dsr.readUint32();
			if ( ds.status() != QDataStream::Ok )
				return true;

			Protocol * const protocol = threadPrivate->handlerManager()->d_->protocolForId( port.protocolId );
			if ( !protocol )
				return false;

			if ( port.bindedTransmitter < 0 || port.bindedTransmitter >= data.transmitters.size() )
				return false;

			if ( port.bindedTransmitter != 0 )
			{
				SnapshotData::Transmitter & transmitter = data.transmitters[ port.bindedTransmitter ];
				if ( transmitter.isNull() )
					return false;

				transmitter.bindedPorts << port.portId;
			}
		}

		for ( IdGeneratorZombieIterator zombiePortsIt( data.portIdGenerator ); zombiePortsIt.hasNext(); )
		{
			const int portId = zombiePortsIt.next();

			data.resizePortsTo( portId, portsLimit );

			SnapshotData::Port & port = data.ports[ portId ];
			if ( !port.isNull() )
				return false;

			port.portId = portId;
			port.protocolId = dsr.readUint32();
			port.bindedTransmitter = 0;

			Protocol * const protocol = threadPrivate->handlerManager()->d_->protocolForId( port.protocolId );
			if ( !protocol )
				return false;
		}
	}

	// read id generators
	{
		data.idGeneratorsGenerator.setIdsLimit( totalIdGeneratorsLimit );
		data.idGeneratorsGenerator.setTransmittersLimit( transmittersLimit );
		data.idGeneratorsGenerator.setZombiesLimit(
			_zombiesLimit( transmittersDelayLimit, totalIdGeneratorsLimit ) );

		ds >> data.idGeneratorsGenerator;
		if ( ds.status() != QDataStream::Ok )
			return true;

		for ( IdGeneratorIterator it( data.idGeneratorsGenerator ); it.hasNext(); )
		{
			const int generatorId = it.next();

			data.resizeIdGeneratorsTo( generatorId, totalIdGeneratorsLimit );

			SnapshotData::IdGeneratorInfo & idGeneratorInfo = data.idGeneratorInfos[ generatorId ];
			if ( !idGeneratorInfo.isNull() )
				return false;

			idGeneratorInfo.id = generatorId;

			idGeneratorInfo.type = dsr.readUint32();
			if ( ds.status() != QDataStream::Ok )
				return true;

			if ( idGeneratorInfo.type < 0 || idGeneratorInfo.type >= idGeneratorTypesCount )
				return false;

			const int idsLimit = threadPrivate->handlerManager()->idsLimit( idGeneratorInfo.type );

			idGeneratorInfo.generator.setIdsLimit( idsLimit );
			idGeneratorInfo.generator.setTransmittersLimit( transmittersLimit );
			idGeneratorInfo.generator.setZombiesLimit( _zombiesLimit( transmittersDelayLimit, idsLimit ) );

			ds >> idGeneratorInfo.generator;
			if ( ds.status() != QDataStream::Ok )
				return true;
		}
	}

	// read hold on flag
	{
		const quint8 transmittersOnHold = dsr.readUint8();
		if ( ds.status() != QDataStream::Ok )
			return true;

		if ( transmittersOnHold != 0 && transmittersOnHold != 1 )
			return false;

		data.transmittersOnHold = transmittersOnHold;
	}

	s = data;

	return true;
#else
	FLAT_FATAL;
#endif
}


bool HandlerPrivate::_readSnapshotDataPendingPacketsInternal( QDataStream & ds, SnapshotData & s,
		const StreamPrivate * const threadPrivate )
{
#if 0
	if ( ds.status() != QDataStream::Ok )
		return true;

	QList<SnapshotData::Packet> pendingPackets;

	// read pending packets
	{
		const int pendingPacketsCount = dsr.readUint32();
		if ( ds.status() != QDataStream::Ok )
			return true;

		if ( pendingPacketsCount < 0 )
			return false;

		for ( int i = 0; i < pendingPacketsCount; ++i )
		{
			SnapshotData::Packet packet;

			packet.portId = dsr.readUint32();
			if ( ds.status() != QDataStream::Ok )
				return true;

			if ( packet.portId <= 0 || packet.portId >= s.ports.size() )
				return false;

			const SnapshotData::Port & port = s.ports.at( packet.portId );
			if ( port.isNull() )
				return false;

			if ( !s.portIdGenerator.isZombie( packet.portId ) && port.bindedTransmitter == 0 )
				return false;

			Protocol * const protocol = threadPrivate->takeProtocolForId( port.protocolId );

			protocol->readPacket( ds, packet.packet );
			if ( ds.status() != QDataStream::Ok )
				return true;

			pendingPackets << packet;
		}
	}

	s.pendingPackets = pendingPackets;

	return true;
}


bool HandlerPrivate::_readSnapshotInternal( QDataStream & ds, Snapshot & s, const StreamPrivate * const threadPrivate )
{
	const int snapshotSize = dsr.readUint32();
	if ( ds.status() != QDataStream::Ok )
		return true;

	if ( snapshotSize < 0 )
		return false;

	if ( ds.device()->bytesAvailable() < snapshotSize )
	{
		ds.setStatus( QDataStream::ReadPastEnd );
		return true;
	}

	// save current stream pos
	const qint64 snapshotStartPos = ds.device()->pos();

	SnapshotData snapshotData;

	if ( !_readSnapshotDataHeaderInternal( ds, snapshotData, threadPrivate ) )
		return false;
	if ( !_readSnapshotDataPendingPacketsInternal( ds, snapshotData, threadPrivate ) )
		return false;
	if ( ds.status() != QDataStream::Ok )
		return true;

	// read handler snapshot
	QVariant handlerSnapshot;
	threadPrivate->handlerManager()->readSnapshot( ds, handlerSnapshot );
	if ( ds.status() != QDataStream::Ok )
		return true;

	// check that snapshotSize matches read bytes count
	const qint64 bytesRead = ds.device()->pos() - snapshotStartPos;
	if ( (qint64)snapshotSize != bytesRead )
		return false;

	s.d_ = new SnapshotPrivate( snapshotData, handlerSnapshot );

	return true;
#else
	FLAT_FATAL;
#endif
}


void HandlerPrivate::readSnapshot( QDataStream & ds, Snapshot & s, const StreamPrivate * const threadPrivate )
{
#if 0
	if ( !_readSnapshotInternal( ds, s, threadPrivate ) )
		ds.setStatus( QDataStream::ReadCorruptData );
#else
	FLAT_FATAL;
#endif
}
#endif




}}}
