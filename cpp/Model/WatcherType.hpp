
#pragma once

#include <string_view>




namespace Flat { namespace Game { namespace Model {




struct _WatcherTypeHandle {};
typedef const _WatcherTypeHandle * WatcherType;

template <typename T>
struct WatcherTraits
{
	static WatcherType type() noexcept;
	static std::string_view name() noexcept;
};




}}}




#define FLAT_GAME_MODEL_DEFINE_WATCHER_TYPE(Type, Name, Export) \
	namespace Flat { namespace Game { namespace Model { \
		template <typename T> Export WatcherType WatcherTraits<T>::type() noexcept \
		{ static const _WatcherTypeHandle t; return &t; }; \
		\
		template <> std::string_view WatcherTraits<Type>::name() noexcept \
		{ return { #Name }; } \
		\
		template struct WatcherTraits<Type>; \
	}}}

#define FLAT_GAME_MODEL_DECLARE_WATCHER_META(Type) \
	Flat::Game::Model::WatcherType watcherType() const noexcept override; \
	static Flat::Game::Model::WatcherType staticWatcherType() noexcept;

#define FLAT_GAME_MODEL_DEFINE_WATCHER_META(Type) \
	Flat::Game::Model::WatcherType Type::watcherType() const noexcept \
	{ return Flat::Game::Model::WatcherTraits<Type>::type(); } \
	\
	Flat::Game::Model::WatcherType Type::staticWatcherType() noexcept \
	{ return Flat::Game::Model::WatcherTraits<Type>::type(); }
