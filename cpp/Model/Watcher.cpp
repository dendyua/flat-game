
#include "Watcher.hpp"
#include "Watcher_p.hpp"

#include "Model_p.hpp"
#include "Debug.hpp"

#include "../Handler_p.hpp"




namespace Flat { namespace Game { namespace Model {




WatcherPrivate::WatcherPrivate() noexcept
{
}


WatcherPrivate::~WatcherPrivate()
{
}


void WatcherPrivate::setModelPrivate(ModelPrivate * const modelPrivate) noexcept
{
	modelPrivate_ = modelPrivate;
}




Watcher::Watcher() noexcept :
	d_(*new WatcherPrivate)
{
}


Watcher::~Watcher()
{
	FLAT_ASSERT(d_->modelPrivate_);
	d_->modelPrivate_->handlerPrivate().uninstallWatcher(*this);
}


const Model & Watcher::model() const noexcept
{
	return d_->modelPrivate_->model();
}




}}}
