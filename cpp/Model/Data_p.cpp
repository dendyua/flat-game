
#include "Data_p.hpp"

#include "Info.hpp"
#include "ProtocolBase.hpp"




namespace Flat { namespace Game { namespace Model {




Data::Data(const Info & info, const PortPool::FreeCallback portFreeCallback) noexcept :
	info(info),
	peerPool(info),
	idPool(peerPool, info.poolLimit(), info.poolReserve(), info.delayLimit(),
			IdPool::FreeCallback()),
	portPool(peerPool, info.portLimit(), info.portReserve(), info.delayLimit(),
			portFreeCallback)
{
}


Data::~Data()
{
}




}}}
