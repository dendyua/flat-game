
#pragma once

#include <map>
#include <vector>
#include <unordered_map>

#include "Info.hpp"




namespace Flat { namespace Game { namespace Model {




class Info::Private
{
public:
	struct WatcherInfo
	{
		WatcherType type = nullptr;
		Info::WatcherAllocator allocator;
	};

	struct PacketInfo
	{
		Info::PacketReader reader;
		Info::PacketWriter writer;
		PacketSize size;
	};

	struct ProtocolInfo
	{
		std::vector<PacketInfo> packets;
	};

	const ProtocolInfo & protocolInfo(ProtocolId protocolId) const noexcept;
	const PacketInfo & packetInfo(ProtocolId protocolId, PacketId packetId) const noexcept;

private:
	int peerLimit_ = 0;
	int portLimit_ = 0;
	int portReserve_ = 0;
	int poolLimit_ = 0;
	int poolReserve_ = 0;
	int delayLimit_ = 1;

	Info::ModelAllocator modelAllocator_;
	std::map<WatcherType, WatcherInfo> watcherInfoForType_;

	std::vector<ProtocolInfo> protocols_;

	friend class Info;
};




inline const Info::Private::ProtocolInfo & Info::Private::protocolInfo(
		const ProtocolId protocolId) const noexcept
{ return protocols_.at(static_cast<size_t>(protocolId)); }

inline const Info::Private::PacketInfo & Info::Private::packetInfo(const ProtocolId protocolId,
		const PacketId packetId) const noexcept
{
	return protocolInfo(protocolId).packets.at(static_cast<size_t>(packetId));
}




}}}
