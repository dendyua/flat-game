
#pragma once

#include <Flat/Utils/Stream.hpp>

#include "Debug.hpp"
#include "PoolId.hpp"




namespace Flat { namespace Game { namespace Model {




enum class PeerId { Null = -1 };

struct _Id;
typedef PoolId<_Id> Id;

struct _PortId;
typedef PoolId<_PortId> PortId;




}}}




namespace Flat { namespace Debug {

template <>
inline void Log::append<Game::Model::PeerId>(const Game::Model::PeerId & id) noexcept
{
	append(static_cast<int>(id));
}

template <>
inline void Log::append<Flat::Game::Model::PortId::Index>(
		const typename Flat::Game::Model::PortId::Index & index) noexcept
{
	append(static_cast<int>(index));
}

template <>
inline void Log::append<Flat::Game::Model::Id::Index>(
		const typename Flat::Game::Model::Id::Index & index) noexcept
{
	append(static_cast<int>(index));
}

}}




FLAT_UTILS_STREAM_DECLARE_ENUM(Game::Model::PeerId, uint32_t)
FLAT_UTILS_STREAM_DECLARE_ENUM(Game::Model::PortId::Index, uint32_t)
