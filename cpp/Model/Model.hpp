
#pragma once

#include <memory>
#include <functional>
#include <any>

#include <Flat/Core/Ref.hpp>
#include <Flat/Utils/Stream.hpp>

#include "Correction.hpp"
#include "Common.hpp"
#include "Protocol.hpp"
#include "Info.hpp"




namespace Flat { namespace Game {
	class Handler;
	class HandlerPrivate;
}}




namespace Flat { namespace Game { namespace Model {




class Context;
class Model;
class ModelPrivate;
class Storage;
class Watcher;




class FLAT_GAME_EXPORT Model
{
public:
	Model(const Context & context) noexcept;
	virtual ~Model();

	const Storage & storage() const noexcept;

	bool isConstructed() const noexcept;

	ModelPrivate & d() noexcept;

protected:
	virtual void construct() = 0;

	virtual std::any snapshot() const;
	virtual bool constructFromSnapshot(const std::any & snapshot);

	virtual void watcherAdded(Watcher & watcher) = 0;
	virtual void watcherRemoved(Watcher & watcher) = 0;

	// ports
	int availablePortCount() const noexcept;
	int reservePortCount() const noexcept;
	template <typename Protocol, typename Handler> PortId createPort(Handler & handler) noexcept;

	// packets
	void advance();

	// memory
	int availableIdCount() const noexcept;
	Id takeId() noexcept;

	// correction
	Correction & correction();

#if 0
	// snapshot bindings
	template <typename P, typename D> bool bindPort(int portId);
	bool bindPool(int type, int poolId);
	bool bindId(int type, int poolId, int id);
#endif

private:
	typedef struct _ProtocolHandlerOpaque * _ProtocolHandlerId;

	template <typename Handler>
	static _ProtocolHandlerId _handlerToId(Handler & handler) noexcept
	{ return reinterpret_cast<_ProtocolHandlerId>(&handler); }

	template <typename Handler>
	static Handler & _handlerFromId(_ProtocolHandlerId id) noexcept
	{ return *reinterpret_cast<Handler*>(id); }

	typedef std::function<void(_ProtocolHandlerId, Utils::Stream::ReaderIf&,
			const Info::PacketReader&)> PacketHandler;

	template <bool Empty, typename Handler, int Index, typename List> struct PacketRegistrator;

private:
	void _freeId(Id::Index index) noexcept;
	void _freePortId(PortId::Index index) noexcept;

	PortId _allocatePort(ProtocolId protocolId, _ProtocolHandlerId handlerId, int count) noexcept;
	void _registerPortPacketHandler(PortId::Index portIndex, PacketId packetId,
			PacketHandler handler) noexcept;

	bool _bindPort(int portId, int protocolId);

private:
	Ref<ModelPrivate> d_;

	friend class ModelPrivate;
	friend class PoolId<_Id>;
	friend class PoolId<_PortId>;
};




inline ModelPrivate & Model::d() noexcept
{ return *d_; }

template <typename Handler, int Index, typename... Types>
struct Model::PacketRegistrator<true, Handler, Index, std::tuple<Types...>>
{
	static void exec(Model &, PortId::Index) {}
};

template <typename T, typename Handler, int Index, typename... Types>
struct Model::PacketRegistrator<false, Handler, Index, std::tuple<T, Types...>>
{
	static void exec(Model & model, const PortId::Index portIndex)
	{
		typedef T Packet;

		constexpr PacketId packetId = static_cast<PacketId>(Index);

		const Model::PacketHandler handler = [] (Model::_ProtocolHandlerId h,
				Utils::Stream::ReaderIf & r, const Info::PacketReader & reader) {
			Handler & handler = _handlerFromId<Handler>(h);
			Packet packet;
			reader(r, packet);
			handler.handle(packet);
		};

		model._registerPortPacketHandler(portIndex, packetId, handler);

		PacketRegistrator<sizeof...(Types) == 0, Handler, Index + 1, std::tuple<Types...>>::exec(model,
				portIndex);
	}
};

template <typename Protocol, typename Handler>
inline PortId Model::createPort(Handler & handler) noexcept
{
	typedef typename ProtocolPacketsInfo<Protocol>::Packets Packets;

	PortId portId = _allocatePort(_protocolId<Protocol>(), Model::_handlerToId(handler),
			std::tuple_size<Packets>::value);

	PacketRegistrator<std::tuple_size<Packets>::value == 0, Handler, 0, Packets>::exec(*this,
			portId.value());

	return portId;
}


#if 0
template <typename P, typename D>
inline bool Handler::bindPort( const int portId )
{
	return _bindPort( portId, _protocolIdForMetaTypeId( Protocol<P, D>::metaTypeId() ) );
}
#endif




template <>
inline void PoolId<_Id>::free() noexcept
{ model_->_freeId(index_); }




template <>
inline void PoolId<_PortId>::free() noexcept
{ model_->_freePortId(index_); }




}}}
