
#pragma once

#include <memory>




namespace Flat { namespace Game { namespace Model {




class SnapshotPrivate;




class FLAT_GAME_EXPORT Snapshot
{
public:
	Snapshot();
	Snapshot(const Snapshot & snapshot);
	Snapshot & operator=(const Snapshot & snapshot);
	~Snapshot();

	bool isNull() const;

private:
	Snapshot(SnapshotPrivate * d);

	std::shared_ptr<SnapshotPrivate> d_;

	friend class HandlerPrivate;
	friend class StreamPrivate;
};




}}}
