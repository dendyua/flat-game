
#pragma once

#include <type_traits>

#include <Flat/Utils/Stream.hpp>

#include "ProtocolBase.hpp"




namespace Flat { namespace Game { namespace Model {





template <typename T> struct SizeOfType;

struct PacketSize
{
	int min, max;
};

template <typename T>
struct _PodSizeOfType
{
	static constexpr int value = sizeof(T);
	static constexpr PacketSize size{value, value};
};




template <typename T>
struct _PacketFieldTypeWrapper
{
	typedef T type;
};




template <typename F, typename... T>
constexpr _PacketSerializationMode _resolveFirstPacketSerializationMode()
{
	return std::is_void<F>::value ? _PacketSerializationMode::Void : _PacketSerializationMode::Tuple;
}

template <int S, typename... T>
struct _PacketSerializationModeResolver
{
	static constexpr _PacketSerializationMode value = _resolveFirstPacketSerializationMode<T...>();
};

template <typename... T>
struct _PacketSerializationModeResolver<0, T...>
{
	static constexpr _PacketSerializationMode value = _PacketSerializationMode::Custom;
};

template <_PacketSerializationMode Mode, typename... T> struct _PacketSerializationTupleResolver;

template <typename... T> struct _PacketSerializationTupleResolver<
		_PacketSerializationMode::Void, T...>
{
	typedef std::tuple<> SerializationTuple;
	typedef std::tuple<> FieldTuple;
};

template <typename... T> struct _PacketSerializationTupleResolver<
		_PacketSerializationMode::Tuple,  T...>
{
	typedef std::tuple<T...> SerializationTuple;
	typedef std::tuple<typename _PacketFieldTypeWrapper<T>::type&...> FieldTuple;
};

template <typename... T> struct _PacketSerializationTupleResolver<
		_PacketSerializationMode::Custom, T...>
{
	typedef void SerializationTuple;
	typedef void FieldTuple;
};




template <typename P, typename... T>
class Packet : public _PacketBase
{
public:
	typedef P Protocol;
	static constexpr _PacketSerializationMode kSerializationMode =
			_PacketSerializationModeResolver<sizeof...(T), T...>::value;
	typedef _PacketSerializationTupleResolver<kSerializationMode, T...> _PacketSerializationTuple;
	typedef typename _PacketSerializationTuple::SerializationTuple SerializationTuple;
	typedef typename _PacketSerializationTuple::FieldTuple Fields;
};




template <typename Packet>
class Serializer
{
public:
	static void write(Utils::Stream::WriterIf & w, const Packet & packet);
	static void read(Utils::Stream::ReaderIf & w, Packet & packet);
};




template <int Size> struct _SerializerEnumIntegral { typedef void type; };
template <> struct _SerializerEnumIntegral<1> { typedef std::uint8_t type; };
template <> struct _SerializerEnumIntegral<2> { typedef std::uint16_t type; };
template <> struct _SerializerEnumIntegral<4> { typedef std::uint32_t type; };
template <> struct _SerializerEnumIntegral<8> { typedef std::uint64_t type; };


template <typename T>
struct _SerializerTupleType
{
	typedef typename std::remove_reference<T>::type Type;
	static constexpr bool IsTypeEnum = std::is_enum<Type>::value;
	typedef typename std::conditional<IsTypeEnum, typename _SerializerEnumIntegral<sizeof(Type)>::type, Type>::type type;
	static_assert(sizeof(type) == sizeof(Type), "Invalid enum integral");

	static type & toType(Type & value) { return reinterpret_cast<type&>(value); }
	static const type & toType(const Type & value) { return reinterpret_cast<const type&>(value); }
};




template <bool IsSame, typename SerializerType, typename FieldType> struct _FieldSerializerWorker;

template <typename SerializerType, typename FieldType>
struct _FieldSerializerWorker<true, SerializerType, FieldType>
{
	static void write(Utils::Stream::WriterIf & w, const FieldType & field) noexcept
	{
		w.write(_SerializerTupleType<FieldType>::toType(field));
	}

	static void read(Utils::Stream::ReaderIf & r, FieldType & field) noexcept
	{
		r.read(_SerializerTupleType<FieldType>::toType(field));
	}
};

template <typename SerializerType, typename FieldType>
struct _FieldSerializerWorker<false, SerializerType, FieldType>
{
	static void write(Utils::Stream::WriterIf & w, const FieldType & field) noexcept
	{
		w.write(SerializerType(field));
	}

	static void read(Utils::Stream::ReaderIf & r, FieldType & field) noexcept
	{
		SerializerType serializer(field);
		r.read(serializer);
	}
};

template <typename SerializerType, typename FieldType> struct _FieldSerializer
{
	static constexpr bool IsSame = std::is_same<SerializerType, FieldType>::value;

	static void write(Utils::Stream::WriterIf & w, const FieldType & field) noexcept
	{ _FieldSerializerWorker<IsSame, SerializerType, FieldType>::write(w, field); }

	static void read(Utils::Stream::ReaderIf & r, FieldType & field) noexcept
	{ _FieldSerializerWorker<IsSame, SerializerType, FieldType>::read(r, field); }
};





template <typename Packet, _PacketSerializationMode Mode> struct _PacketSerializer;

template <typename Packet>
struct _PacketSerializer<Packet, _PacketSerializationMode::Void>
{
	static void write(Utils::Stream::WriterIf & w, const Packet &)
	{
	}

	static void read(Utils::Stream::ReaderIf & r, Packet &)
	{
	}
};

template <bool Valid, int Index, typename SerializationTuple, typename Fields>
struct _TupleSerializer {};

template <int Index, typename SerializationTuple, typename Fields>
struct _TupleSerializer<false, Index, SerializationTuple, Fields>
{
	static void write(Utils::Stream::WriterIf &, const Fields &) {}
	static void read(Utils::Stream::ReaderIf &, Fields &) {}
};

template <int Index, typename SerializationTuple, typename Fields>
struct _TupleSerializer<true, Index, SerializationTuple, Fields>
{
	typedef typename std::tuple_element<Index, Fields>::type RefFieldType;
	typedef typename std::remove_reference<RefFieldType>::type FieldType;
	typedef typename std::tuple_element<Index, SerializationTuple>::type SerializationType;

	static constexpr int kNextIndex = Index + 1;
	static constexpr bool kHasNextElement = kNextIndex < std::tuple_size<Fields>::value;
	typedef _TupleSerializer<kHasNextElement, kNextIndex, SerializationTuple, Fields> NextElement;

	static void write(Utils::Stream::WriterIf & w, const Fields & fields)
	{
		_FieldSerializer<SerializationType, FieldType>::write(w, std::get<Index>(fields));
		NextElement::write(w, fields);
	}

	static void read(Utils::Stream::ReaderIf & r, Fields & fields)
	{
		_FieldSerializer<SerializationType, FieldType>::read(r, std::get<Index>(fields));
		NextElement::read(r, fields);
	}
};

template <typename Packet>
struct _PacketSerializer<Packet, _PacketSerializationMode::Tuple>
{
	typedef typename Packet::SerializationTuple SerializationTuple;
	typedef typename Packet::Fields Fields;

	static void write(Utils::Stream::WriterIf & w, const Packet & p)
	{
		Fields fields = const_cast<Packet&>(p).fields();
		_TupleSerializer<true, 0, SerializationTuple, Fields>::write(w, fields);
	}

	static void read(Utils::Stream::ReaderIf & r, Packet & p)
	{
		Fields fields = p.fields();
		_TupleSerializer<true, 0, SerializationTuple, Fields>::read(r, fields);
	}
};

template <typename Packet>
struct _PacketSerializer<Packet, _PacketSerializationMode::Custom>
{
	static void write(Utils::Stream::WriterIf & w, const Packet & p)
	{
		Serializer<Packet>::write(w, p);
	}

	static void read(Utils::Stream::ReaderIf & r, Packet & p)
	{
		Serializer<Packet>::read(r, p);
	}
};




template <typename T>
struct _SizeOfTypeResolver
{
	typedef typename std::conditional<std::is_pod<T>::value, _PodSizeOfType<T>, SizeOfType<T>>::type type;
};




template <bool Empty, typename... Types> struct _SizeOfTuple;

template <typename... Types>
struct _SizeOfTuple<true, Types...>
{
	static constexpr PacketSize size{0, 0};
};

template <typename T>
struct _SizeOfTuple<false, std::tuple<T>>
{
	static constexpr PacketSize size = _SizeOfTypeResolver<T>::type::size;
};

template <typename T, typename...Types >
struct _SizeOfTuple<false, std::tuple<T, Types...>>
{
	typedef typename _SizeOfTypeResolver<T>::type S;
	typedef _SizeOfTuple<sizeof...(Types) == 0, std::tuple<Types...>> Next;

	static constexpr PacketSize size{
			S::size.min + Next::size.min, S::size.max + Next::size.max};
};




template <bool Custom, typename P, typename... Types> struct _PacketSizeResolver;

template <typename P, typename Tuple>
struct _PacketSizeResolver<true, P, Tuple>
{
	static constexpr PacketSize size = P::kPacketSize;
};

template <typename P, typename Tuple>
struct _PacketSizeResolver<false, P, Tuple>
{
	static constexpr PacketSize size = _SizeOfTuple<std::tuple_size<Tuple>::value == 0, Tuple>::size;
};

template <typename P>
struct _PacketSize
{
	static constexpr PacketSize size =
			_PacketSizeResolver<P::kSerializationMode == _PacketSerializationMode::Custom, P,
				typename P::SerializationTuple>::size;
};




}}}
