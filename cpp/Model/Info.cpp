
#include "Info.hpp"
#include "Info_p.hpp"

#include "Debug.hpp"
#include "Watcher.hpp"
#include "Model.hpp"
#include "Context_p.hpp"




namespace Flat { namespace Game { namespace Model {




#if 0
struct Info::ProtocolDescriptor::Private
{
	Info::Private::ProtocolInfo protocolInfo;
};




Info::ProtocolDescriptor::ProtocolDescriptor(const _ProtocolToken token) noexcept :
	d(*new Private)
{
	d->protocolInfo.protocolToken = token;
	FLAT_DEBUG << token << d->protocolInfo.protocolToken;
}


Info::ProtocolDescriptor::~ProtocolDescriptor()
{
}


void Info::ProtocolDescriptor::add(const _PacketToken token, const PacketReader reader,
		const PacketWriter writer) noexcept
{
	Info::Private::PacketInfo packetInfo;
	packetInfo.packetToken = token;
	packetInfo.reader = std::move(reader);
	packetInfo.writer = std::move(writer);

	const PacketId packetId = static_cast<PacketId>(d->protocolInfo.packets.size());
	d->protocolInfo.packetIdForToken[token] = packetId;
	d->protocolInfo.packets.push_back(std::move(packetInfo));
}
#endif




Info::Info() noexcept :
	d_(*new Private)
{
}


Info::~Info()
{
}


int Info::peerLimit() const noexcept
{
	return d_->peerLimit_;
}


void Info::setupPeers(const int limit) noexcept
{
	FLAT_ASSERT(limit >= 0);
	d_->peerLimit_ = limit;
}


int Info::portLimit() const noexcept
{
	return d_->portLimit_;
}


int Info::portReserve() const noexcept
{
	return d_->portReserve_;
}


void Info::setupPorts(const int limit, const int reserve) noexcept
{
	FLAT_ASSERT(limit >= 0 && reserve >= 0);
	d_->portLimit_ = limit;
	d_->poolReserve_ = reserve;
}


int Info::poolLimit() const noexcept
{
	return d_->poolLimit_;
}


int Info::poolReserve() const noexcept
{
	return d_->poolReserve_;
}


void Info::setupPool(const int limit, const int reserve) noexcept
{
	FLAT_ASSERT(limit >= 0 && reserve >= 0);
	d_->poolLimit_ = limit;
	d_->poolReserve_ = reserve;
}


int Info::delayLimit() const noexcept
{
	return d_->delayLimit_;
}


void Info::setupDelay(const int limit) noexcept
{
	FLAT_ASSERT(limit >= 1);
	d_->delayLimit_ = limit;
}


std::unique_ptr<Context> Info::_context(Handler & handler) const noexcept
{
	return std::unique_ptr<Context>(new ContextPrivate(*this, handler));
}


void Info::_registerModel(const ModelAllocator allocator) noexcept
{
	d_->modelAllocator_ = allocator;
}


void Info::_registerWatcher(const WatcherType watcherType,
		const WatcherAllocator allocator) noexcept
{
	Private::WatcherInfo & watcherInfo = d_->watcherInfoForType_[watcherType];
	FLAT_ASSERT(!watcherInfo.type);

	watcherInfo.type = watcherType;
	watcherInfo.allocator = allocator;
}


void Info::_allocateProtocols(const int count) noexcept
{
	FLAT_ASSERT(d_->protocols_.empty());
	d_->protocols_.resize(count);
}


void Info::_allocateProtocolPackets(const ProtocolId protocolId, const int count) noexcept
{
	Private::ProtocolInfo & protocolInfo = d_->protocols_[static_cast<size_t>(protocolId)];
	FLAT_ASSERT(protocolInfo.packets.empty());
	protocolInfo.packets.resize(count);
}


void Info::_registerProtocolPacket(const ProtocolId protocolId, const PacketId packetId,
	const PacketReader reader, const PacketWriter writer, const PacketSize size) noexcept
{
	Private::ProtocolInfo & protocolInfo = d_->protocols_[static_cast<size_t>(protocolId)];
	Private::PacketInfo & packetInfo = protocolInfo.packets[static_cast<size_t>(packetId)];
	packetInfo.reader = reader;
	packetInfo.writer = writer;
	packetInfo.size = size;
}


#if 0
void Info::_registerProtocol(ProtocolDescriptor & protocolDescriptor) noexcept
{
	const ProtocolId protocolId = static_cast<ProtocolId>(d_->protocols_.size());
	d_->protocolIdForToken_[protocolDescriptor.d->protocolInfo.protocolToken] = protocolId;
	d_->protocols_.push_back(std::move(protocolDescriptor.d->protocolInfo));
}
#endif


std::unique_ptr<Model> Info::createModel(Handler & handler) const noexcept
{
	return std::unique_ptr<Model>(d_->modelAllocator_(handler));
}


std::unique_ptr<Watcher> Info::createWatcher(const WatcherType watcherType) const noexcept
{
	const Info::Private::WatcherInfo & watcherInfo = d_->watcherInfoForType_.at(watcherType);
	return std::unique_ptr<Watcher>(watcherInfo.allocator());
}




}}}
