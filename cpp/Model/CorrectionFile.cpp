
#include "CorrectionFile.hpp"

#include <QFile>




namespace Flat { namespace Game { namespace Model {




class CorrectionFilePrivate
{
public:
	enum ActionCode
	{
		Action_Open  = 1,
		Action_Close = 2,
		Action_Seek  = 3,
		Action_Read  = 4
	};

	CorrectionFilePrivate(Correction & correction);

public:
	Correction & correction_;
	QFile file_;
};




inline CorrectionFilePrivate::CorrectionFilePrivate(Correction & correction) :
	correction_(correction)
{
}




CorrectionFile::CorrectionFile(Correction & correction) :
	d_(*new CorrectionFilePrivate(correction))
{
}


CorrectionFile::~CorrectionFile()
{
}


QString CorrectionFile::fileName() const
{
	return d_->file_.fileName();
}


void CorrectionFile::setFileName(const QString & fileName)
{
	d_->file_.setFileName( fileName );
}


bool CorrectionFile::open(const OpenMode mode)
{
	const bool isOpened = d_->file_.open(mode);

	d_->correction_ << CorrectionFilePrivate::Action_Open;
	d_->correction_ << isOpened;
	d_->correction_ << d_->file_.size();

	return isOpened;
}


void CorrectionFile::close()
{
	d_->correction_ << CorrectionFilePrivate::Action_Close;

	d_->file_.close();
}


bool CorrectionFile::atEnd() const
{
	return d_->file_.atEnd();
}


qint64 CorrectionFile::bytesAvailable() const
{
	return d_->file_.bytesAvailable();
}


qint64 CorrectionFile::bytesToWrite() const
{
	return d_->file_.bytesToWrite();
}


bool CorrectionFile::canReadLine() const
{
	return d_->file_.canReadLine();
}


bool CorrectionFile::isSequential() const
{
	return d_->file_.isSequential();
}


qint64 CorrectionFile::pos() const
{
	return d_->file_.pos();
}


qint64 CorrectionFile::size() const
{
	return d_->file_.size();
}


bool CorrectionFile::reset()
{
	return d_->file_.reset();
}


bool CorrectionFile::seek(const qint64 pos)
{
	return d_->file_.seek(pos);
}


qint64 CorrectionFile::readData(char * const data, const qint64 maxlen)
{
	const qint64 readSize = d_->file_.read(data, maxlen);

	d_->correction_.stream().writeRawData(data, static_cast<int>(readSize));

	return readSize;
}


qint64 CorrectionFile::writeData(const char * const data, const qint64 len)
{
	return d_->file_.write(data, len);
}




}}}
