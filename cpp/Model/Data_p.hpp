
#pragma once

#include "Pool_p.hpp"
#include "PeerPool_p.hpp"
#include "Common.hpp"
#include "ProtocolBase.hpp"




namespace Flat { namespace Game { namespace Model {




class Info;




typedef Pool<PortId> PortPool;
typedef Pool<Id> IdPool;




class Data
{
public:
	Data(const Info & info, PortPool::FreeCallback portFreeCallback) noexcept;
	~Data();

	struct PortInfo
	{
		ProtocolId protocolId = ProtocolId::Null;
	};

	const PortInfo & portInfoAt(PortId::Index portIndex) const noexcept;
	PortInfo & portInfoAt(PortId::Index portIndex) noexcept;

public:
	const Info & info;

	PeerPool peerPool;

	PortPool portPool;

	IdPool idPool;

	std::vector<PortInfo> portInfos;
};




inline const Data::PortInfo & Data::portInfoAt(const PortId::Index portIndex) const noexcept
{ return portInfos.at(static_cast<size_t>(portIndex)); }

inline Data::PortInfo & Data::portInfoAt(const PortId::Index portIndex) noexcept
{ return portInfos[static_cast<size_t>(portIndex)]; }




}}}
