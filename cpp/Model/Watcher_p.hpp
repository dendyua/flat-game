
#pragma once




namespace Flat { namespace Game { namespace Model {




class ModelPrivate;




class WatcherPrivate
{
public:
	WatcherPrivate() noexcept;
	~WatcherPrivate();

	void setModelPrivate(ModelPrivate * modelPrivate) noexcept;

private:
	ModelPrivate * modelPrivate_ = nullptr;

	friend class Watcher;
};




}}}
