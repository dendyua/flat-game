
#include <QIODevice>

#include <Flat/Core/Ref.h>

#include "Correction.hpp"




namespace Flat { namespace Game { namespace Model {




class Correction;

class CorrectionFilePrivate;




class FLAT_GAME_EXPORT CorrectionFile : public QIODevice
{
	Q_OBJECT

public:
	CorrectionFile(Correction & correction);
	~CorrectionFile();

	QString fileName() const;
	void setFileName(const QString & fileName);

	bool open(OpenMode mode) override;
	void close() override;

	bool atEnd() const override;

	qint64 bytesAvailable() const override;
	qint64 bytesToWrite() const override;
	bool canReadLine() const override;
	bool isSequential() const override;
	qint64 pos() const override;
	qint64 size() const override;

	bool reset() override;
	bool seek(qint64 pos) override;

private:
	qint64 readData(char * data, qint64 maxlen) override;
	qint64 writeData(const char * data, qint64 len) override;

private:
	Ref<CorrectionFilePrivate> d_;
};




}}}
