
#pragma once

#include <functional>
#include <vector>

#include "Debug.hpp"
#include "Common.hpp"




class QDataStream;




namespace Flat { namespace Game { namespace Model {




class PeerPool;




class PoolBase
{
public:
	enum class Index { Null = static_cast<int>(PoolId<void>::Null) };

	typedef std::function<void(Index)> FreeCallback;

	enum class IdMode
	{
		Free,
		Taken,
		Zombie
	};

	PoolBase(const PeerPool & peerPool, int limit, int reserve, int delayLimit,
			FreeCallback freeCallback) noexcept;

	// optimization
	int enlargeCount() const noexcept;
	void setEnlargeCount(int count) noexcept;

	// dynamic parameters
	void peerRemoved(PeerId peerId) noexcept;

	void advance() noexcept;
	void advancePeer(PeerId peerId) noexcept;

	Index take() noexcept;
	void free(Index id) noexcept;

	int count() const noexcept;
	int availableCount() const noexcept;
	int reserveCount() const noexcept;

	bool validate(PeerId peerId, Index id) const noexcept;
	bool isZombie(Index id) const noexcept;

private:
	class IdInfo
	{
	public:
		Index next;
		Index previous;
		IdMode mode;

		uint32_t takenTimestamp;
		uint32_t zombieTimestamp;
	};

	const IdInfo & _at(Index id) const noexcept;
	IdInfo & _at(Index id) noexcept;

	int _totalLimit() const noexcept;
	int _freeCount() const noexcept;
	void _enlarge() noexcept;
	void _freeZombiesAtDelay(int delay) noexcept;
	bool _readDataStream(QDataStream & ds) noexcept;

private:
	const PeerPool & peerPool_;

	// configuration
	const int limit_;
	const int reserve_;
	const int delayLimit_;
	const FreeCallback freeCallback_;

	// ids
	Index firstFree_;
	Index firstTaken_;
	Index firstZombie_;
	Index lastZombie_;
	std::vector<IdInfo> ids_;

	// helpers
	int takenCount_;
	int zombieCount_;

	int enlargeCount_;

	uint32_t timestamp_;

	friend QDataStream & operator<<(QDataStream & ds, const PoolBase & pool);
	friend QDataStream & operator>>(QDataStream & ds, PoolBase & pool);

	template <IdMode> friend class PoolIteratorBase;
};




template <typename T>
class Pool : public PoolBase
{
public:
	typedef typename T::Index Index;
	typedef std::function<void(Index)> FreeCallback;

	Pool(const PeerPool & peerPool, int limit, int reserve, int delayLimit,
			FreeCallback freeCallback) noexcept :
		PoolBase(peerPool, limit, reserve, delayLimit, freeCallback ?
				[this, freeCallback](PoolBase::Index i){freeCallback(_castFromIndex(i));} :
				PoolBase::FreeCallback())
	{}

	Index take() noexcept { return _castFromIndex(PoolBase::take()); }
	void free(Index i) noexcept { PoolBase::free(_castToindex(i)); }

	bool validate(PeerId peerId, Index i) const noexcept { return PoolBase::validate(peerId, _castToindex(i)); }
	bool isZombie(Index i) const noexcept { return PoolBase::isZombie(_castToindex(i)); }

private:
	PoolBase::Index _castToindex(Index i) const noexcept { return static_cast<PoolBase::Index>(i); }
	Index _castFromIndex(PoolBase::Index i) const noexcept { return static_cast<Index>(i); }
};




template <PoolBase::IdMode Mode>
class PoolIteratorBase
{
public:
	static constexpr PoolBase::IdMode kMode = Mode;

	PoolIteratorBase(const PoolBase & pool) noexcept;

	bool atEnd() const noexcept;
	PoolBase::Index next() noexcept;

private:
	const PoolBase & pool_;
	PoolBase::Index next_;
};




template <typename I, PoolBase::IdMode Mode>
class PoolIterator : public PoolIteratorBase<Mode>
{
public:
	PoolIterator(const Pool<I> & pool) noexcept : PoolIteratorBase<Mode>(pool) {}
	I next() noexcept { return static_cast<I>(PoolIteratorBase<Mode>::next()); }
};




inline int PoolBase::_totalLimit() const noexcept
{ return limit_ + reserve_; }

inline int PoolBase::enlargeCount() const noexcept
{ return enlargeCount_; }

inline void PoolBase::setEnlargeCount(const int count) noexcept
{ FLAT_ASSERT(count == -1 || count > 0); enlargeCount_ = count; }

inline const PoolBase::IdInfo & PoolBase::_at(const Index id) const noexcept
{ return ids_.at(static_cast<int>(id)); }

inline PoolBase::IdInfo & PoolBase::_at(const Index id) noexcept
{ return ids_[static_cast<int>(id)]; }




template <>
inline PoolIteratorBase<PoolBase::IdMode::Free>::PoolIteratorBase(const PoolBase & pool) noexcept :
	pool_(pool),
	next_(pool_.firstFree_)
{}

template <>
inline PoolIteratorBase<PoolBase::IdMode::Taken>::PoolIteratorBase(const PoolBase & pool) noexcept :
	pool_(pool),
	next_(pool_.firstTaken_)
{}

template <>
inline PoolIteratorBase<PoolBase::IdMode::Zombie>::PoolIteratorBase(const PoolBase & pool) noexcept :
	pool_(pool),
	next_(pool_.firstZombie_)
{}

template <PoolBase::IdMode Mode>
inline bool PoolIteratorBase<Mode>::atEnd() const noexcept
{ return next_ == PoolBase::Index::Null; }

template <PoolBase::IdMode Mode>
inline PoolBase::Index PoolIteratorBase<Mode>::next() noexcept
{
	FLAT_ASSERT(!atEnd());

	const PoolBase::Index value = next_;
	next_ = pool_._at(value).next;
	return value;
}




}}}




namespace Flat { namespace Debug {

template <>
inline void Log::append<Game::Model::PoolBase::IdMode>(
		const Game::Model::PoolBase::IdMode & mode) noexcept
{
	switch (mode) {
	case Game::Model::PoolBase::IdMode::Free:   appendString("Free"); return;
	case Game::Model::PoolBase::IdMode::Taken:  appendString("Taken"); return;
	case Game::Model::PoolBase::IdMode::Zombie: appendString("Zombie"); return;
	}

	printf("(Invalid mode: %d)", static_cast<int>(mode));
	maybeSpace();
}

}}
