
#pragma once

#include <Flat/Core/Ref.hpp>

#include "WatcherType.hpp"




namespace Flat { namespace Game {
	class HandlerPrivate;
}}




namespace Flat { namespace Game { namespace Model {




class WatcherPrivate;
class Model;




class FLAT_GAME_EXPORT Watcher
{
public:
	Watcher() noexcept;
	virtual ~Watcher();

	const Model & model() const noexcept;

	virtual WatcherType watcherType() const noexcept = 0;

	template <typename W>
	W * cast() noexcept
	{
		if (watcherType() != WatcherTraits<W>::type()) return nullptr;
		return static_cast<W*>(this);
	}

private:
	Ref<WatcherPrivate> d_;

	friend class Flat::Game::HandlerPrivate;
};




}}}
