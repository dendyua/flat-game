
#pragma once

#include <Flat/Utils/Stream.hpp>

#include "Debug.hpp"
#include "Common.hpp"




namespace Flat { namespace Game { namespace Model { namespace RouterProtocol {




struct CorruptError {};




enum class PacketType
{
	PeerCreated    = 0,
	PeerDestroyed  = 1,
	PeerPortPacket = 2,
	PeerAdvanced   = 3,

	Max
};




template<PacketType t>
class BasePacket
{
public:
	static constexpr PacketType kType = t;

	constexpr PacketType packetType() const noexcept
	{ return t; }
};




class PeerCreatedPacket : public BasePacket<PacketType::PeerCreated>
{
public:
	PeerCreatedPacket() noexcept
	{}

	explicit PeerCreatedPacket(PeerId peerId) noexcept :
		peerId(peerId)
	{}

	PeerId peerId;
};




class PeerDestroyedPacket : public BasePacket<PacketType::PeerDestroyed>
{
public:
	PeerDestroyedPacket() noexcept
	{}

	explicit PeerDestroyedPacket(PeerId peerId) noexcept :
		peerId(peerId)
	{}

	PeerId peerId;
};




class PeerPortPacket : public BasePacket<PacketType::PeerPortPacket>
{
public:
	PeerPortPacket() noexcept
	{}

	PeerPortPacket(PeerId peerId, PortId::Index portIndex, int packetSize) noexcept :
		peerId(peerId),
		portIndex(portIndex),
		packetSize(packetSize)
	{}

	PeerId peerId;
	PortId::Index portIndex;
	int packetSize;
};




class PeerAdvancedPacket : public BasePacket<PacketType::PeerAdvanced>
{
public:
	PeerAdvancedPacket() noexcept
	{}

	explicit PeerAdvancedPacket(PeerId peerId) noexcept :
		peerId(peerId)
	{}

	PeerId peerId;
};




}}}}




FLAT_UTILS_STREAM_DECLARE_TYPE(Game::Model::RouterProtocol::PacketType)
FLAT_UTILS_STREAM_DECLARE_TYPE(Game::Model::RouterProtocol::PeerCreatedPacket)




namespace Flat { namespace Debug {

template <>
inline void Log::append<Game::Model::RouterProtocol::PacketType>(
		const Game::Model::RouterProtocol::PacketType & packetType) noexcept
{
	switch (packetType) {
	case Game::Model::RouterProtocol::PacketType::PeerCreated:    appendString( "PeerCreated" ); return;
	case Game::Model::RouterProtocol::PacketType::PeerDestroyed:  appendString( "PeerDestroyed" ); return;
	case Game::Model::RouterProtocol::PacketType::PeerPortPacket: appendString( "PeerPortPacket" ); return;
	case Game::Model::RouterProtocol::PacketType::PeerAdvanced:   appendString( "PeerAdvanced" ); return;
	case Game::Model::RouterProtocol::PacketType::Max: break;
	}

	printf("(Invalid model packet type: %d)", static_cast<uint32_t>(packetType));
	maybeSpace();
}

}}
