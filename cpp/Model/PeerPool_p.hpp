
#pragma once

#include <vector>

#include "Common.hpp"
#include "PeerHistory_p.hpp"




namespace Flat { namespace Game { namespace Model {




class Info;




class PeerPool
{
public:
	class Iterator
	{
	public:
		Iterator(const PeerPool & pool) noexcept;
		bool atEnd() const noexcept;
		PeerId next();

	private:
		const PeerPool & pool_;
		PeerId next_;
	};

	PeerPool(const Info & info) noexcept;

	int count() const noexcept;

	void create(PeerId id) noexcept;
	void destroy(PeerId id) noexcept;

	bool isValid(PeerId id) const noexcept;

	int delay() const noexcept;
	int delayForPeer(PeerId id) const noexcept;
	void advance() noexcept;
	void advancePeer(PeerId id) noexcept;

	int peerCountAtDelay(int delay) const noexcept;

private:
	struct PeerInfo
	{
		PeerId next;
		PeerId previous;
		int delay = 0;
		bool isCorrupted = false;

		// assert helper
		bool isNull = true;
	};

	class State
	{
	public:
		int peerCount = 0;
	};

private:
	int _limit() const noexcept;
	const PeerInfo & _infoForPeer(PeerId peerId) const noexcept;
	PeerInfo & _infoForPeer(PeerId peerId) noexcept;
	void _enlarge(int maxIndex) noexcept;

private:
	const Info & info_;
	std::vector<PeerInfo> peerInfos_;
	PeerId first_ = PeerId::Null;
	PeerId firstNull_ = PeerId::Null;
	int count_ = 0;

	PeerHistory<State> states_;
};




inline int PeerPool::count() const noexcept
{ return count_; }

inline int PeerPool::delay() const noexcept
{ return states_.count(); }

inline int PeerPool::delayForPeer(const PeerId id) const noexcept
{ return peerInfos_.at(static_cast<int>(id)).delay; }

inline const PeerPool::PeerInfo & PeerPool::_infoForPeer(const PeerId peerId) const noexcept
{ return peerInfos_.at(static_cast<int>(peerId)); }

inline PeerPool::PeerInfo & PeerPool::_infoForPeer(const PeerId peerId) noexcept
{ return peerInfos_[static_cast<int>(peerId)]; }

inline int PeerPool::peerCountAtDelay(const int delay) const noexcept
{ return states_.at(delay).peerCount; }




}}}
