
#pragma once

#include "Model/Common.hpp"




namespace Flat { namespace Game {




enum class SessionId : std::uint64_t {};

struct Session
{
	Session() noexcept {}

	Session(const Session & s) noexcept :
		peerId(s.peerId),
		sessionId(s.sessionId)
	{}

	Session(Model::PeerId peerId, SessionId sessionId) noexcept :
		peerId(peerId),
		sessionId(sessionId)
	{}

	bool isNull() const noexcept
	{ return peerId == Model::PeerId::Null; }

	Model::PeerId peerId = Model::PeerId::Null;
	SessionId sessionId;
};




}}




namespace Flat { namespace Debug {

template <>
inline void Log::append<Game::SessionId>(const Game::SessionId & id) noexcept
{
	append(static_cast<std::uint32_t>(id));
}

template <>
inline void Log::append<Game::Session>(const Game::Session & session) noexcept
{
	*this << "Session{" << NoSpaceOnce() << session.peerId << SpaceOnce()
			<< session.sessionId << NoSpaceOnce() << "}";
}

}}
