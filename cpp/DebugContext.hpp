
#pragma once

#include <Flat/Debug/Debug.hpp>

FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(FlatGame, FLAT_GAME_SOURCE_DIR "/cpp", true)
FLAT_DEBUG_DECLARE_SECONDARY_DEBUG_CONTEXT(FlatGame, 1, FLAT_GAME_SOURCE_DIR "/include/Flat/Game")
