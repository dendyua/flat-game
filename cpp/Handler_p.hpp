
#pragma once

#include <memory>
#include <set>
#include <mutex>
#include <condition_variable>

#include <Flat/Core/Ref.hpp>
#include <Flat/Utils/BufferStream.hpp>

#include "Model/Snapshot.hpp"
#include "Model/Common.hpp"
#include "Model/WatcherType.hpp"

#include "Handler.hpp"




namespace Flat { namespace Game {




class Handler;

namespace Model {
	class Info;
	class Snapshot;
	class Model;
	class Watcher;
}

class StreamPrivate;




class HandlerPrivate
{
public:
	HandlerPrivate(Handler & handler, const Model::Info & info, Handler::Callbacks & callbacks,
			const Model::Storage & storage, const Model::Snapshot & snapshot) noexcept;
	~HandlerPrivate();

	const Model::Info & info() const noexcept;
	const Model::Storage & storage() const noexcept;

	void installSender(Model::PortId::Index portIndex, SenderBase & sender) noexcept;
	void uninstallSender(Model::PortId::Index portIndex, SenderBase & sender) noexcept;

	Model::ProtocolId protocolIdForPort(Model::PortId::Index portIndex) const noexcept;

	void sendPacket(Model::PortId::Index portIndex, const Model::PacketId packetId,
			const Model::_PacketBase & packet, SenderOutput & output);

	void start(Flat::Utils::Stream & input);

	void portCreated(Model::PortId::Index portIndex) noexcept;
	void portDestroyed(Model::PortId::Index portIndex) noexcept;

	std::unique_ptr<Model::Watcher> installWatcher(Model::WatcherType watcherType) noexcept;
	void uninstallWatcher(Model::Watcher & watcher) noexcept;

	void peerCreated(Model::PeerId peerId) noexcept;
	void peerDestroyed(Model::PeerId peerId) noexcept;

private:
	struct PortInfo
	{
		SenderBase * sender = nullptr;
	};

private:
	void _readInput() noexcept;
	void _commitPacket();

private:
	Handler & handler_;
	const Model::Info & info_;
	Handler::Callbacks & callbacks_;
	const Model::Storage & storage_;
	const Model::Snapshot snapshot_;

	struct InputCallbacks : Flat::Utils::Stream::ReaderCallbacks {
		InputCallbacks(HandlerPrivate & h) noexcept : h(h) {}
		HandlerPrivate & h;
		void ready() override { h._readInput(); }
	} inputCallbacks_;

	Flat::Utils::Stream * input_ = nullptr;

	std::unique_ptr<Model::Model> model_;

	std::set<Model::Watcher*> watchers_;

	int senderCount_ = 0;
	std::vector<PortInfo> portInfos_;

	// packet helpers
	Utils::BufferStream packetBuffer_;
};




inline const Model::Info & HandlerPrivate::info() const noexcept
{ return info_; }

inline const Model::Storage & HandlerPrivate::storage() const noexcept
{ return storage_; }




}}
