
#pragma once

#include <Flat/Core/Ref.hpp>

#include "Model/Common.hpp"
#include "Model/ProtocolBase.hpp"




namespace Flat { namespace Game {




class Handler;
class HandlerPrivate;




struct SenderOutput
{
	virtual void begin() = 0;
	virtual void end() = 0;
	virtual void write(const uint8_t *, int) = 0;
};




class FLAT_GAME_EXPORT SenderBase
{
public:
	~SenderBase();

	Model::PortId::Index port() const noexcept;

private:
	SenderBase(HandlerPrivate & handlerPrivate, Model::ProtocolId protocolId,
			Model::PortId::Index portIndex, SenderOutput & output) noexcept;

	void _send(Model::PacketId packetId, const Model::_PacketBase & p);

private:
	class Private;
	Ref<Private> d_;

	template <typename P> friend class Sender;
};




template <typename P>
class Sender : public SenderBase
{
public:
	typedef P Protocol;

	template <typename Packet>
	void send(const Packet & packet)
	{
		static_assert(std::is_same<Protocol, typename Packet::Protocol>::value,
				"Packet does not belong to protocol");
		static_assert(std::is_base_of<Model::_PacketBase, Packet>::value,
				"Packet is not derived from Packet class");

		constexpr Model::PacketId packetId = Model::_packetId<Packet>();
		SenderBase::_send(packetId, packet);
	}

private:
	Sender(HandlerPrivate & handlerPrivate, Model::PortId::Index portIndex,
			SenderOutput & output) noexcept :
		SenderBase(handlerPrivate, Model::_protocolId<Protocol>(), portIndex, output)
	{}

	friend class Handler;
};




}}
