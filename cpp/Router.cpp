
#include "Router.hpp"
#include "Router_p.hpp"

#include "Model/RouterProtocol_p.hpp"

#include "Debug.hpp"




namespace Flat { namespace Game {




template <typename T>
inline Utils::Stream::Writer RouterPrivate::_writePacket(const T & packet)
{
	Utils::Stream::Writer w = output_.lockForWrite();

	typedef typename Model::RouterProtocol::BasePacket<T::kType> Base;
	const Base & base = static_cast<const Base&>(packet);

	w << base.packetType();
	w << packet;

	return w;
}




RouterPrivate::RouterPrivate(const int peerLimit, const int delayLimit, Router & router) noexcept :
	delayLimit_(delayLimit),
	router_(router),
	peerIdGenerator_(peerLimit)
{
	FLAT_ASSERT(peerIdGenerator_.limit() >= 0);
	FLAT_ASSERT(delayLimit_ >= 0);
}


RouterPrivate::~RouterPrivate()
{
}


void RouterPrivate::_resizePeersTo(const Model::PeerId peerId) noexcept
{
	FLAT_ASSERT(static_cast<int>(peerId) < peerIdGenerator_.limit());

	if (static_cast<size_t>(peerId) < peerInfoForId_.size()) return;

	if (static_cast<size_t>(peerId) >= peerInfoForId_.capacity()) {
		peerInfoForId_.reserve(std::min<size_t>(static_cast<int>(peerId) + 16, peerIdGenerator_.limit()));
	}

	peerInfoForId_.resize(static_cast<size_t>(peerId) + 1);
}


void RouterPrivate::_advance() noexcept
{
	FLAT_ASSERT(delay() < delayLimit_);

	// increase delay for all peers
	for (Utils::IdGenerator::Iterator it(peerIdGenerator_); it.hasNext();) {
		const Model::PeerId peerId = static_cast<Model::PeerId>(it.next());
		PeerInfo & peerInfo = _peerInfo(peerId);
		FLAT_ASSERT(!peerInfo.isNull());
		peerInfo.delay++;
	}

	State state;
	state.peerCount = peerIdGenerator_.count();
	stateHistory_.insert(stateHistory_.begin(), state);
}


void RouterPrivate::_advancePeer(PeerInfo & peerInfo) noexcept
{
	if (peerInfo.delay == 0) {
		_advance();
	}

	State & state = stateHistory_[peerInfo.delay - 1];
	FLAT_ASSERT(state.peerCount > 0);
	state.peerCount--;

	if (state.peerCount == 0) {
		FLAT_ASSERT(peerInfo.delay == delay());
		stateHistory_.pop_back();
	}

	peerInfo.delay--;
}


int RouterPrivate::delay() const noexcept
{
	return stateHistory_.size();
}


int RouterPrivate::delayForPeer(const Model::PeerId peerId) const noexcept
{
	const PeerInfo & peerInfo = _peerInfo(peerId);
	FLAT_ASSERT(!peerInfo.isNull());
	return peerInfo.delay;
}


Session RouterPrivate::createSession() noexcept
{
	FLAT_ASSERT(!peerIdGenerator_.isFull()) << "Peer limit reached";

	const Model::PeerId peerId = static_cast<Model::PeerId>(peerIdGenerator_.take());
	FLAT_ASSERT(static_cast<int>(peerId) >= 0
			&& static_cast<int>(peerId) < peerIdGenerator_.limit());

	_resizePeersTo(peerId);

	const SessionId sessionId = static_cast<SessionId>(currentSessionFrame_);
	currentSessionFrame_++;

	const Session session(peerId, sessionId);

	std::unique_ptr<SessionInfo> sessionInfo(new SessionInfo);
	sessionInfo->session = session;

	PeerInfo & peerInfo = _peerInfo(peerId);
	FLAT_ASSERT(peerInfo.isNull());
	peerInfo.sessionInfo = sessionInfo.get();

	sessionInfoForId_[session.sessionId] = std::move(sessionInfo);

	const Model::RouterProtocol::PeerCreatedPacket peerCreatedPacket =
			Model::RouterProtocol::PeerCreatedPacket(peerId);
	_writePacket(peerCreatedPacket);

	return session;
}


void RouterPrivate::destroySession(const Session & session) noexcept
{
	PeerInfo & peerInfo = _peerInfo(session.peerId);
	FLAT_ASSERT(!peerInfo.isNull() && peerInfo.sessionInfo->session.sessionId == session.sessionId);

	while (peerInfo.delay != 0) {
		_advancePeer(peerInfo);
	}

	peerIdGenerator_.free(static_cast<int>(session.peerId));

	currentSessionFrame_++;

	peerInfo.sessionInfo->isDestroyed = true;
	peerInfo.sessionInfo->destroyFrame = currentSessionFrame_;

	peerInfo = PeerInfo();

	const Model::RouterProtocol::PeerDestroyedPacket peerDestroyedPacket(session.peerId);
	_writePacket(peerDestroyedPacket);
}


RouterPrivate::SessionInfo * RouterPrivate::_sessionInfoForSession(
		const Session & session) const noexcept
{
	if (peerIdGenerator_.isTaken(static_cast<int>(session.peerId))) {
		const PeerInfo & peerInfo = _peerInfo(session.peerId);

		if (peerInfo.sessionInfo->session.sessionId == session.sessionId) {
			return peerInfo.sessionInfo;
		}
	}

	const auto it = sessionInfoForId_.find(session.sessionId);
	if (it == sessionInfoForId_.end()) return nullptr;
	if (it->second->session.peerId != session.peerId) return nullptr;
	return it->second.get();
}


bool RouterPrivate::validateSession(const Session & session, const bool advancing) const noexcept
{
	if (static_cast<int>(session.peerId) < 0
			|| static_cast<int>(session.peerId) >= peerIdGenerator_.limit()) {
		return false;
	}

	SessionInfo * const sessionInfo = _sessionInfoForSession(session);
	if (!sessionInfo) return false;

	const std::int32_t delay = static_cast<std::int32_t>(
			currentSessionFrame_ - sessionInfo->frame());
	if (advancing) {
		return delay > 0;
	} else {
		return delay >= 0;
	}
}


void RouterPrivate::advanceSession(const Session & session) noexcept
{
	FLAT_ASSERT(validateSession(session, true));

	SessionInfo * const sessionInfo = _sessionInfoForSession(session);
	FLAT_ASSERT(sessionInfo);

	sessionInfo->advanceFrameCounter++;

	if (sessionInfo->isDestroyed) {
		if (sessionInfo->frame() == sessionInfo->destroyFrame) {
			sessionInfoForId_.erase(sessionInfoForId_.find(session.sessionId));
		}
	}
}


void RouterPrivate::sendPeerPacket(const Session & session,
		const Model::PortId::Index portIndex, const uint8_t * const data, const int size) noexcept
{
	FLAT_ASSERT(validateSession(session, false));

	SessionInfo * const sessionInfo = _sessionInfoForSession(session);
	FLAT_ASSERT(sessionInfo);
	if (sessionInfo->isDestroyed) return;

	const Model::RouterProtocol::PeerPortPacket peerPortPacket(session.peerId, portIndex, size);

	Flat::Utils::Stream::Writer writer = _writePacket(peerPortPacket);
	writer->writeData(data, size);
}


void RouterPrivate::advancePeer(const Session & session) noexcept
{
	FLAT_ASSERT(validateSession(session, false));

	SessionInfo * const sessionInfo = _sessionInfoForSession(session);
	FLAT_ASSERT(sessionInfo);
	if (sessionInfo->isDestroyed) return;

	PeerInfo & peerInfo = _peerInfo(session.peerId);
	FLAT_ASSERT(!peerInfo.isNull());

	FLAT_ASSERT(peerInfo.delay > 0);
	FLAT_ASSERT(peerInfo.delay <= delay());

	_advancePeer(peerInfo);

	const Model::RouterProtocol::PeerAdvancedPacket peerAdvancedPacket(session.peerId);
	_writePacket(peerAdvancedPacket);
}




Router::Router(const int peerLimit, const int delayLimit) noexcept :
	d_(*new RouterPrivate(peerLimit, delayLimit, *this))
{
}


Router::~Router()
{
}


Utils::Stream & Router::output()
{
	return d_->output();
}


int Router::peerCount() const noexcept
{
	return d_->peerCount();
}


int Router::delay() const noexcept
{
	return d_->delay();
}


int Router::delayForPeer(const Model::PeerId peerId) const noexcept
{
	return d_->delayForPeer(peerId);
}


SessionId Router::currentSessionId() const noexcept
{
	return d_->currentSessionId();
}


Session Router::createSession() noexcept
{
	return d_->createSession();
}


void Router::destroySession(const Session & session) noexcept
{
	d_->destroySession(session);
}


bool Router::validateSession(const Session & session, const bool advancing) const noexcept
{
	return d_->validateSession(session, advancing);
}


void Router::advanceSession(const Session & session) noexcept
{
	d_->advanceSession(session);
}


void Router::sendPeerPacket(const Session & session, const Model::PortId::Index portIndex,
		const uint8_t * const data, const int size) noexcept
{
	d_->sendPeerPacket(session, portIndex, data, size);
}


void Router::advancePeer(const Session & session) noexcept
{
	d_->advancePeer(session);
}




}}
