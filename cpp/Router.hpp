
#pragma once

#include <memory>

#include <Flat/Core/Ref.hpp>

#include "Model/Common.hpp"

#include "Session.hpp"




class QIODevice;

namespace Flat { namespace Utils {
	class Stream;
}}




namespace Flat { namespace Game {




class RouterPrivate;




class FLAT_GAME_EXPORT Router
{
public:
	Router(int peerLimit, int delayLimit) noexcept;
	~Router();

	Utils::Stream & output();

	int peerCount() const noexcept;
	int delay() const noexcept;
	int delayForPeer(Model::PeerId peerId) const noexcept;

	SessionId currentSessionId() const noexcept;

	Session createSession() noexcept;
	void destroySession(const Session & session) noexcept;
	bool validateSession(const Session & session, bool advancing) const noexcept;
	void advanceSession(const Session & session) noexcept;
	void sendPeerPacket(const Session & session, Model::PortId::Index portIndex,
			const uint8_t * data, int size) noexcept;
	void advancePeer(const Session & session) noexcept;

private:
	Ref<RouterPrivate> d_;

	friend class RouterPrivate;
};




}}
