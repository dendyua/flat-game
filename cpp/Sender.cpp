
#include "Sender.hpp"

#include "Model/Info.hpp"

#include "Debug.hpp"
#include "Handler_p.hpp"




namespace Flat { namespace Game {




class SenderBase::Private
{
public:
	Private(HandlerPrivate & handlerPrivate,
			Model::PortId::Index portIndex, SenderOutput & output) noexcept :
		handlerPrivate(handlerPrivate),
		portIndex(portIndex),
		output(output)
	{}

	HandlerPrivate & handlerPrivate;
	const Model::PortId::Index portIndex;
	SenderOutput & output;
};




SenderBase::SenderBase(HandlerPrivate & handlerPrivate, const Model::ProtocolId protocolId,
		const Model::PortId::Index portIndex, SenderOutput & output) noexcept :
	d_(*new Private(handlerPrivate, portIndex, output))
{
	FLAT_ASSERT(handlerPrivate.protocolIdForPort(portIndex) == protocolId);

	d_->handlerPrivate.installSender(d_->portIndex, *this);
}


SenderBase::~SenderBase()
{
	d_->handlerPrivate.uninstallSender(d_->portIndex, *this);
}


Model::PortId::Index SenderBase::port() const noexcept
{
	return d_->portIndex;
}


void SenderBase::_send(const Model::PacketId packetId, const Model::_PacketBase & p)
{
	d_->handlerPrivate.sendPacket(d_->portIndex, packetId, p, d_->output);
}




}}
